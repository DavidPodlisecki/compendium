<?php
/**
 * The template for displaying the footer
 *
 * @package WordPress
 * @subpackage MU
 * @since MU 1.0
 */
?>

<div id="footer">
  <div class="footerContent">
    <div class="column3">
      <h1>Location</h1>
      <p><strong>Marquette University </strong><br />
        <a href="/visit/">1250 W. Wisconsin Ave. </a><br />
        Milwaukee, WI 53233<br />
        <strong>Phone: </strong>(800) 222-6544</p>
    </div>
    <div class="column3">
      <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Custom Footer') ) : ?><?php endif; ?>

      <?php /*if(!get_field('custom-footer-links')): ?>
      <h1>Key Resources</h1>
      <ul class="columnsLeft">
        <li><a href="/tools/campus-contacts.php">Campus contacts</a></li>
        <li><a href="/visit/map.php">Campus map</a></li>
        <li><a href="/tools/contact-us.php">Contact us</a></li>
        <li><a href="/search/">Search marquette.edu</a></li>
      </ul>
      <ul class="columnsRight">
        <li><a href="https://emarq.marquette.edu">eMarq</a></li>
        <li><a href="https://checkmarq.mu.edu">CheckMarq</a></li>
        <li><a href="https://d2l.mu.edu">D2L</a></li>
        <li><a href="https://myjob.mu.edu">MyJob</a></li>
      </ul>
      <?php else: ?>
      <h1><?php if(get_field('custom-footer-header')): the_field('custom-footer-header'); else: ?>Key Resources<?php endif; ?></h1>
      <ul class="columnsLeft">
        <?php if(get_field('custom-footer-links')): $countLinks = 0; ?>
        <?php while(has_sub_field('custom-footer-links')): $countLinks++; ?>
        <?php if($countLinks == 5): ?>
      </ul>
      <ul class="columnsRight">
      <?php endif; ?>
        <li><a href="<?php the_sub_field('link-url'); ?>"><?php the_sub_field('link-text'); ?></a></li>
        <?php endwhile; ?>
      </ul>
      <?php endif; ?>
      <?php endif;*/ ?>


    </div>
    <div class="column3 siteindex last">
      <h1>MARQUETTE A to Z</h1>
      <p><a href="/tools/atoz.php?index=a">A</a><a href="/tools/atoz.php?index=b">B</a><a href="/tools/atoz.php?index=c">C</a><a href="/tools/atoz.php?index=d">D</a><a href="/tools/atoz.php?index=e">E</a><a href="/tools/atoz.php?index=f">F</a><a href="/tools/atoz.php?index=g">G</a><a href="/tools/atoz.php?index=h">H</a><a href="/tools/atoz.php?index=i">I</a><a href="/tools/atoz.php?index=j">J</a><a href="/tools/atoz.php?index=k">K</a><a href="/tools/atoz.php?index=l">L</a><a href="/tools/atoz.php?index=m">M</a> <a href="/tools/atoz.php?index=n"> N</a><a href="/tools/atoz.php?index=o">O</a><a href="/tools/atoz.php?index=p">P</a><a href="/tools/atoz.php?index=q">Q</a><a href="/tools/atoz.php?index=r">R</a><a href="/tools/atoz.php?index=s">S</a><a href="/tools/atoz.php?index=t">T</a><a href="/tools/atoz.php?index=u">U</a><a href="/tools/atoz.php?index=v">V</a><a href="/tools/atoz.php?index=w">W</a><a href="/tools/atoz.php?index=xyz">X</a><a href="/tools/atoz.php?index=xyz">Y</a><a href="/tools/atoz.php?index=xyz">Z</a></p>
    </div>
    <br class="float_clear" />
  </div>
  <div id="lowerFooter">
    <div class="footerContent"> <a href="http://www.marquette.edu"><img alt="Marquette University logo" src="<?php bloginfo('template_directory'); ?>/images/marquette-logo-transparent-white.png"></a>
      <p><a href="/tools/privacy.php">Privacy Policy</a> <a href="/tools/legal.php">Legal Disclaimer</a> <a href="/tools/non-discrimination.php">Non-Discrimination Policy</a></p>
      <p> &copy; 2013 Marquette University</p>
      <br class="float_clear" />
    </div>
  </div>
</div>
</div>
<?php wp_footer(); ?>
<?php if(is_page_template('tmpl-custom.php')): ?>
<script language="javascript" type="text/javascript">
  $(document).ready(function(){
    $('#mainImage').bxSlider({
      controls: false,
      mode:'fade'
    });
  });
</script>
<?php elseif(is_page_template('tmpl-search.php')): ?>
<script language="javascript" type="text/javascript">

var goodData = false;
var iso = null;

$(document).ready(function() {
  // quick search regex
  var qsRegex;   
  var buttonFilter = '*';
  var collegeFilter = '*';
  var archiveFilter = '*';
  var allresults = false;
  
  $('#noresults').hide();
  
  // init Isotope
  var $grid = $('#content').isotope({
    itemSelector: '.element-item',
    layoutMode: 'masonry',
    //sortBy : 'random',
    filter: function() {
      var $this = $(this);
      var searchResult = qsRegex ? $this.text().match( qsRegex ) : true;
      var buttonResult = buttonFilter ? $this.is( buttonFilter ) : true;
      var collegeResult = collegeFilter ? $this.is( collegeFilter ) : true;
      var archiveResult = archiveFilter ? $this.is( archiveFilter ) : true;
      //var dateResult = dateFilter ? $this.is( dateFilter ) : true;
      //console.log(buttonResult);
      return searchResult && buttonResult && collegeResult && archiveResult;
    }
  });

  console.log("$grid", $grid);





          // use value of search field to filteri
          var $quicksearch = $('#quicksearch').keyup( debounce( function() {
          	$('#noresults').hide();
           	checkresults();
            qsRegex = new RegExp( $quicksearch.val(), 'gi' );
            
            $grid.delay(1000).isotope();
          }) );

          $('#achievementpulldown').on( 'change', function() {
          	$('#noresults').hide();
            checkresults();
            buttonFilter =  $(this).val();
            $grid.delay(1000).isotope();
            
          });
           $('#archive-dropdown').on( 'change', function() {
           $('#noresults').hide();
           	checkresults();
            var selIndx = $(this).find('option:selected');
            var realIndex = selIndx.index();
            var dateFilter = selIndx.text().replace(/ /g,'');
            if (dateFilter == '*') {
              archiveFilter = dateFilter;
               
            }
            else {
              archiveFilter = '.'+dateFilter;
              
            }
             $grid.delay(1000).isotope();
          
          });
          $('#college').on( 'change', function() {
          	$('#noresults').hide();
            checkresults();
            collegeFilter =  ".category-" + $(this).val();
            
            $grid.delay(1000).isotope();

          });
 
 


function onArrange() {
            numitems =  $('.element-item:visible').length;
            console.log(numitems);
        	if (numitems == 0) {
        		  $('#noresults').show();
        	}
}
// bind event listener
$grid.on( 'arrangeComplete', onArrange );


 
 
 
 <?php
 
$published_posts = wp_count_posts()->publish;
$posts_per_page = get_option('posts_per_page');
$total_pages = ceil($published_posts / $posts_per_page);

 ?>
 
        var count = 2;
        loadArticle(1);
        var totalnumpages = <?php echo $total_pages; ?>;
        //console.log(totalnumpages);
          $(window).scroll(function(){
          
          		if (!allresults){
                  if  ($(window).scrollTop() == $(document).height() - $(window).height()){

                     
					if (count > totalnumpages){
           				 return false;
        			}else{
            			loadArticle(count);
                     	count++;
        			}
                     
                  }
                 } 
          }); 
 
 
          function loadArticle(pageNumber,numposts) {
            //loadArticle1(pageNumber,numposts);
            // loadArticle2(pageNumber,numposts);
            loadArticleCache(pageNumber,numposts);
            return false;
          }
 
 
          function loadArticle1(pageNumber,numposts){    
                  $('a#inifiniteLoader').show('fast');
                  $.ajax({
                      url: "<?php bloginfo('wpurl') ?>/wp-admin/admin-ajax.php",
                      type:'POST',
                      data: "action=infinite_scroll&page_no="+ pageNumber + '&loop_file=loop&posts='+ numposts, 
                      success: function(thehtml){
                        $('a#inifiniteLoader').hide('1000');
                        //$("#content").append(html);    // This will be the div where our content will be loaded
                        //$grid.isotope(); 
                        //$grid.isotope( 'insert', html )
                        //alert('dude');

                        var node = $('<div/>', { html: thehtml });
                        console.log("thehtml: ", thehtml);
                        console.log("node: ", node);
                        $('#content').isotope( 'insert', node );
                      }
                  });
              return false;
            }
 
          function loadArticle2(pageNumber,numposts){    
                  $('a#inifiniteLoader').show('fast');
                  $.ajax({
                      url: "<?php bloginfo('wpurl') ?>/wp-admin/admin-ajax.php",
                      type:'POST',
                      data: "action=infinite_scroll&page_no="+ pageNumber + '&loop_file=loopJson&posts='+ numposts, 
                      success: function(json){
                        $('a#inifiniteLoader').hide('1000');
                        
                        var results = JSON.parse(json);
                        console.log("results", results);
                        var html = "";

                        for (var i = 0; i < results.length; i++) {
                          var target = results[i];
                          html = html + '<div id="post-' + target.id + '" class="class2 element-item ' + target.class + '">';
                          html = html + '    <h2 class="author">' + target.author + '</h2>';
                          html = html + '    <h3 class="author-title">' + target.title + '</h3>';
                          html = html + '    <div class="date-info">';
                          html = html + '        <p class="date">' + target.date + '</p>';
                          html = html + '        <p class="activity">' + target.activity + '</p>';
                          html = html + '    </div>';
                          html = html + '    <div class="show-less post-txt">' + target.bio + '</div>';
                          html = html + '</div>';
                        }

                        var node = $('<div/>', { html: html });
                        $('#content').isotope( 'insert', node );
                        //console.log("json html", html);
                        console.log("json node", node);
                      }
                  });
              return false;
            }

            function loadArticleCache(pageNumber,numposts){   
              console.log("goodData", goodData);
              if (!goodData) { 
                  $('a#inifiniteLoader').show('fast');
                  $.ajax({
                      url: "/cache.json",
                      type:'GET',
                      success: function(json){
                        $('a#inifiniteLoader').hide('1000');
                        console.log("json", json);
                        //var results = JSON.parse(json);
                        var results = json;
                        console.log("results", results);
                        var html = "";
                        //var htmlArr = [];

                        // var setLength = 1000;
                        // var sets = results.length / setLength;
                        // var start = 0;
                        // console.log("sets", sets);
                        // for (var i = 0; i < sets; i++) {
                        //   var end = start + setLength;
                        //   if (results.length < end) {
                        //     end = results.length;
                        //   }
                        //   console.log("set " + i + " // start: " + start + " // end: " + end);

                        //   for (var j = start; j < end; j++) {
                        //     var target = results[j];
                        //     html = html + '<div id="post-' + target.id + '" class="class2 element-item ' + target.class + '">';
                        //     html = html + '    <h2 class="author">' + target.author + '</h2>';
                        //     html = html + '    <h3 class="author-title">' + target.title + '</h3>';
                        //     html = html + '    <div class="date-info">';
                        //     html = html + '        <p class="date">' + target.date + '</p>';
                        //     html = html + '        <p class="activity">' + target.activity + '</p>';
                        //     html = html + '    </div>';
                        //     html = html + '    <div class="show-less post-txt">' + target.bio + '</div>';
                        //     html = html + '</div>';
                        //   }
                        //   var node = $('<div/>', { html: html });
                        //   $('#content').isotope( 'appended', node );


                        //   // reset for next set
                        //   start = end;
                        // }

                        // add all
                        for (var i = 0; i < results.length; i++) {
                          var target = results[i];
                          // var temp = "";
                          html = html + '<div id="post-' + target.id + '" class="class2 element-item ' + target.class + '">';
                          html = html + '    <h2 class="author">' + target.author + '</h2>';
                          html = html + '    <h3 class="author-title">' + target.title + '</h3>';
                          html = html + '    <div class="date-info">';
                          html = html + '        <p class="date">' + target.date + '</p>';
                          html = html + '        <p class="activity">' + target.activity + '</p>';
                          html = html + '    </div>';
                          html = html + '    <div class="show-less post-txt">' + target.bio + '</div>';
                          html = html + '</div>';
                          //htmlArr.push(temp);
                        }
                        var node = $('<div/>', { html: html });
                        $('#content').isotope( 'insert', node );
                        // $('#content').isotope( 'insert', htmlArr );
                      }
                  });
                  goodData = true;
                  allresults = true;
                }
              return false;
            }

 
 		function checkresults()
 		{
 			console.log("allresults", allresults);
 			if (!allresults){
 				
 					var $isoContainer = $('#content');
					$isoContainer.isotope('remove', $isoContainer.isotope('getItemElements'));

					loadArticle(1,-1);
					allresults = true;
				
			}
			
 		}
 
});

// debounce so filtering doesn't happen every millisecond
function debounce( fn, threshold ) {
  var timeout;
  return function debounced() {
    if ( timeout ) {
      clearTimeout( timeout );
    }
    function delayed() {
      fn();
      timeout = null;
    }
    setTimeout( delayed, threshold || 100 );
  };
}


   
   
   

</script>
<?php endif; ?>
<?php if(is_page_template('tmpl-authors.php')): ?>
<script language="javascript" type="text/javascript">
  $(document).ready(function(){
     // quick search regex
  var qsRegex;
  var collegeFilter = '*';

  // init Isotope
  var $grid = $('.isotopeFaculty').isotope({
    itemSelector: '.faculty',
    layoutMode: 'masonry',
    //sortBy : 'random',
    filter: function() {
      var $this = $(this);
      var searchResult = qsRegex ? $this.text().match( qsRegex ) : true;
      var collegeResult = collegeFilter ? $this.is( collegeFilter ) : true;
      //var dateResult = dateFilter ? $this.is( dateFilter ) : true;
      return collegeResult && searchResult;
    }
  });


      
        var $quicksearch = $('#quicksearch').keyup( ( function() {
           qsRegex = new RegExp( $quicksearch.val(), 'gi' );
            $grid.isotope();
          }) );

          $('#college').on( 'change', function() {
            //collegeFilter =  $(this).val();
			collegeFilter =  ".category-" + $(this).val();

            $grid.isotope(); 
            //console.log (collegeFilter);
          });
 
});
</script>
<?php endif ?>
<style>
.element {
  ...
  /* add this. prefixes for compabtibility */
  transform:translate3d(0,0,0);
  -webkit-transform:translate3d(0,0,0);
  -moz-transform:translate3d(0,0,0);
}
</style>
</body>
</html>