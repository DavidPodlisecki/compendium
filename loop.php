<?php

if (have_posts()) : 
  $counter = 0;
	while (have_posts()) : the_post();

		$thefield = get_field('achievement_type');
		$thedate = get_the_date('M d Y');
		$tagdate = get_the_date('FY');
		$start_year = get_field('start_year');
		$end_year= get_field('end_year');
		$current_year = date("Y");
		$start_month =get_field('start_month');
		$end_month = get_field('end_month');



	if (($thefield=='Reviewed') ||  ($thefield=='Edited') ||  ($thefield=='Elected') ||  ($thefield=='Appointed') ||  ($thefield=='Served') ||  ($thefield=='Organized'))
	{
	
		if ($start_year == "")  
		{
			if ($end_year == "")  
			{
				$start_year = $current_year;
				$end_year = $current_year;
				
			} else { $start_year = $end_year; } 
		}
	
		if ($end_year == "")  
		{
			$end_year = $start_year;
		}
	
	
		if ($start_month == "") 
		{
			$start_month = "January";
			
			if ($end_month =="")
			{		
				$end_month = "December";
			}
		}
		  

		if ($end_month =="")
		{		
			if ($start_month == "January")
			{	
				$end_month = "December";
		
			} else {$end_month = $start_month;}
		}

		if ($start_year != $end_year)  
		{			
 			$thedate = $start_month . " " . $start_year . " - " . $end_month . " " . $end_year; 
 		}
 		else 
 		{
 			if ($end_month != $start_month) {
 				if (($start_month == "January") && ($end_month == "December") ) {
 				$thedate = $start_year;
 				}else {  $thedate = $start_month . " " . $start_year . " - " . $end_month . " " . $end_year;  }
 			} else {$thedate = $start_month ." " . $end_year; }
 		}
 		//determine months to tag
 		

 
 


$el_start_month = explode(" ", $start_month);
$el_end_month = explode(" ", $end_month);

   $begin = DateTime::createFromFormat('j-M-Y', '1-' . $el_start_month[0] .'-'. $start_year);
	

    $end = DateTime::createFromFormat('j-M-Y', '28-' . $el_end_month[0] .'-'. $end_year);

		
		
		$interval = new DateInterval('P1M');
		$daterange = new DatePeriod($begin, $interval ,$end);
		
		$tagdate = '';
		foreach($daterange as $date){
			$tagdate .= $date->format("MdY") . " ";
		}
 
 		
	}
      
      ?>
      

      <div id="post-<?php the_ID(); ?>" class="<?php echo 'class2 element-item ' . $thefield . ' ' . $tagdate;      
          $cat = get_the_author_meta('_author_cat');
  if (empty($cat) || count($cat) <= 0 || !is_array($cat))
  {
  }
  else
  { 
  
	echo ' category-'; echo get_cat_slug($cat[0] );	echo ' category-'; echo get_cat_slug($cat[1] );	


  }?>">
        <h2 class="author"><?php the_author();?></h2>
        <?php
        $author_id = get_the_author_meta('ID');
        $author_title = get_field('title', 'user_'. $author_id );
        ?>

        
        <h3 class="author-title"><?php echo $author_title ?></h3>
        <div class="date-info">
          <p class="date"><?php echo $thedate; ?></p>
          <p class="activity"><?php the_field('achievement_type'); ?></p>
        </div> 


          <?php if (get_field('achievement_type')=='Grant') {?>
             <div class="show-less post-txt">Grant: <?php the_field('grant_amount'); ?>, <?php the_title(); ?> <?php the_field('awarding_organization'); if (get_field('collaborators')!='') {?>, <?php the_field('collaborators');  } ?></div> 
          <?php } elseif (get_field('achievement_type')=='Published-Book') {?>
             <div class="show-less post-txt"><?php if (get_field('first_author')!='') {?>First Author: <?php the_field('first_author'); ?>, <? } ?>Published Book: <?php the_title(); if (get_field('collaborators')!='') {?> <?php the_field('collaborators'); }?></div> 
          <?php } elseif (get_field('achievement_type')=='Published-Chapter') {?>
             <div class="show-less post-txt"><?php if (get_field('first_author')!='') {?>First Author: <?php the_field('first_author'); ?>, <? } ?>Published Chapter: <?php the_title(); if (get_field('collaborators')!='') {?> <?php the_field('collaborators'); }?></div>
          <?php } elseif (get_field('achievement_type')=='Published-Article') {?>
             <div class="show-less post-txt"><?php if (get_field('first_author')!='') {?>First Author: <?php the_field('first_author'); ?>, <? } ?>Published Article: <?php the_title(); if (get_field('collaborators')!='') {?> <?php the_field('collaborators'); }?></div> 
          <?php } elseif (get_field('achievement_type')=='Translated-Book') {?>
             <div class="show-less post-txt"><?php if (get_field('first_author')!='') {?>First Author: <?php the_field('first_author'); ?>, <? } ?>Translated Book: <?php the_title(); if (get_field('collaborators')!='') {?> <?php the_field('collaborators'); }?></div> 
          <?php } elseif (get_field('achievement_type')=='Translated-Chapter') {?>
             <div class="show-less post-txt"><?php if (get_field('first_author')!='') {?>First Author: <?php the_field('first_author'); ?>, <? } ?>Translated Chapter: <?php the_title(); if (get_field('collaborators')!='') {?> <?php the_field('collaborators'); }?></div> 
          <?php } elseif (get_field('achievement_type')=='Translated-Article') {?>
             <div class="show-less post-txt"><?php if (get_field('first_author')!='') {?>First Author: <?php the_field('first_author'); ?>, <? } ?>Translated Article: <?php the_title(); if (get_field('collaborators')!='') {?> <?php the_field('collaborators'); }?></div> 
          <?php } elseif (get_field('achievement_type')=='Reviewed') {?>
             <div class="show-less post-txt">Reviewed: <?php the_title(); if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div> 
          <?php } elseif (get_field('achievement_type')=='Edited') {?>
             <div class="show-less post-txt">Edited: <?php the_title(); if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div> 
          <?php } elseif (get_field('achievement_type')=='Presented') {?>
             <div class="show-less post-txt">Presented:  <?php the_title(); echo ", ";  echo get_the_date('F, Y');  if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div> 
          <?php } elseif (get_field('achievement_type')=='Chaired') {?>
             <div class="show-less post-txt">Chaired: <?php the_title(); if (get_field('collaborators')!='') { the_field('collaborators'); }?>, <? echo get_the_date('F, Y'); ?>.</div>
          <?php } elseif (get_field('achievement_type')=='Awarded') {?>
             <div class="show-less post-txt">Awarded: <?php the_title(); ?>, <?php the_field('awarding_organization'); ?>, <?php echo get_the_date('Y'); if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div>
          <?php } elseif (get_field('achievement_type')=='Elected') {?>
             <div class="show-less post-txt">Elected: <?php the_title(); ?>, <?php echo $thedate; if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div>
          <?php } elseif (get_field('achievement_type')=='Appointed') {?>
             <div class="show-less post-txt">Appointed: <?php the_title(); ?>, <?php echo $thedate; if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div>
          <?php } elseif (get_field('achievement_type')=='Inducted') {?>
             <div class="show-less post-txt">Inducted: <?php the_title(); ?>, <?php echo get_the_date('F, Y'); ?> ," <?php if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div>
          <?php } elseif (get_field('achievement_type')=='Served') {?>
             <div class="show-less post-txt">Served: <?php the_title(); ?>, <?php echo $thedate; ?> </div>
          <?php } elseif (get_field('achievement_type')=='Consulted') {?>
             <div class="show-less post-txt">Consulted: <?php the_title(); ?>, <?php echo get_the_date('F, Y'); ?> ," <?php if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div>
          <?php } elseif (get_field('achievement_type')=='Organized') {?>
             <div class="show-less post-txt">Organized: <?php the_title(); ?>, <?php echo $thedate; if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div>
          <?php } elseif (get_field('achievement_type')=='Patents') {?>
             <div class="show-less post-txt">Patents:  <?php the_title(); ?>," <?php if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div>
          <?php } else { ?>
             <div class="show-less post-txt"><?php the_title(); ?></div> 
          <?php } ?>

      </div>


<?php
  $counter = $counter + 1; 
	endwhile;
  echo "<!--- qk: counter: " . $counter . " --->";
endif; 
?>
