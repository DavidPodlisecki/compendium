<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage MU
 * @since MU 1.0
 */
?>




<?php 
the_title(); 
the_content( );
wp_link_pages();
?>
