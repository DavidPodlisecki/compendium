<?php
/**
 * Template Name: Full Width Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="main-content" class="main-content">
  <div id="primary" class="content-area">
    <div id="content" class="site-content" role="main">

      <div id="pageIdentity"> 
        <!-- Breadcrumbs -->
        <div id="breadcrumbs">
          <p><a href="http://www.marquette.edu">Marquette.edu</a> //  <a href="<?php echo get_site_url(); ?>">Research and Scholarship</a> //</p>
        </div>
         <!-- Page name -->
        <div id="pageName">
             <?php while ( have_posts() ) : the_post();  ?>
     		  <h1><?php the_title();?></h1>
     </div>
    </div>
	 <?php the_content();?>
	<?php endwhile ?>
    </div> 
       
    
    
   
   
  </div>

</div>
</div>
</div>
<?php
get_footer();
