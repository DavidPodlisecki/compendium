<?php
/**
 * Template Name: News Listing
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage MU
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<!-- Left content column -->
    <div id="leftContent">
      <div id="pageIdentity"> 
        <!-- Breadcrumbs -->
        <div id="breadcrumbs">
          <p><a href="http://www.marquette.edu">Marquette.edu</a> //  <a href="<?php echo get_site_url(); ?>">Research and Scholarship</a> //</p>
        </div>
         <!-- Page name -->
        <div id="pageName">
  
     </div>
    </div>

	
<article>

		<?php // Display blog posts on any page @ http://m0n.co/l
		$temp = $wp_query; $wp_query= null;
		$wp_query = new WP_Query(); $wp_query->query('showposts=5' . '&paged='.$paged);
		while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

		<h2><a href="<?php the_permalink(); ?>" title="Read more"><?php the_title(); ?></a></h2>
		<?php the_excerpt(); ?>

		<?php endwhile; ?>

		<?php if ($paged > 1) { ?>

		<nav id="nav-posts">
			<div class="prev"><?php next_posts_link('&laquo; Previous Posts'); ?></div>
			<div class="next"><?php previous_posts_link('Newer Posts &raquo;'); ?></div>
		</nav>

		<?php } else { ?>

		<nav id="nav-posts">
			<div class="prev"><?php next_posts_link('&laquo; Previous Posts'); ?></div>
		</nav>

		<?php } ?>

		<?php wp_reset_postdata(); ?>

	</article>


	





    </div> 
       
    <!-- End left content --> 
    
    <!-- Start sidebar content -->
    <div id="sidebarRightImage">
      <div id="pageImage">
     
      <?php if ( has_post_thumbnail() ) { the_post_thumbnail('single_page-thumb'); 
	  		} else { ?>
	  		<img src="<?php bloginfo('template_directory'); ?>/images/ft-img-placeholder.jpg"/>
	  <?php } ?>  
      </div>
      <div id="sidebarRight">
        <div id="columnHeader">
          <h1> Quick links</h1>
        </div>
        <div id="content">
          <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Page Sidebar') ) : ?><?php endif; ?>
        </div>
      </div>
      <br class="float_clear" />
    </div>
    <!-- End right sidebar --> 
    <br class="float_clear"/>
  </div>

</div>
<?php
get_footer();
