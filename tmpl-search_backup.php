<?php
/**
 * Template Name: Search Backup
 * Description: Custom search template
 *
 * @package WordPress
 * @subpackage MU-v2
 * @since MU 1.0
 */

get_header(); ?>
<!-- Left content column -->
    <div id="wideContent">
      <div id="breadcrumbs">
        <a href="http://marquette.edu/">Marquette.edu</a> //
        <?php if($post->post_parent != false): ?>
        <a href="<?php the_permalink(wp_get_post_parent_id()); ?>"><?php echo get_the_title(wp_get_post_parent_id()); ?></a> //
        <?php endif; ?>
      </div>
      <div id="pageName">
        <h1><?php the_title(); ?></h1>
      </div>

        <!-- Main story item -->
      <?php
      if ( have_posts() ) :
        // Start the Loop.
        while ( have_posts() ) : the_post();

          /*
           * Include the post format-specific template for the content. If you want to
           * use this in a child theme, then include a file called called content-___.php
           * (where ___ is the post format) and that will be used instead.
           */
         the_content();

         ?>
        <p>
          <input type="text" id="quicksearch" placeholder="Search">
        </p>
      <div id="filters" class="button-group filter-button-group">
        <button class="button" data-filter="*">Show All</button>
         <?php

         wp_list_categories(array(
            'title_li'      => __(''),
            'style'         => 'none',
            'exclude'       => '1,4,11,12,13',
            'walker'        => new CatWalk
          )
        );

        ?>
      </div>
      <?php
        endwhile;
      endif;
      ?>


      <?php $achievement = new WP_Query('posts_per_page=-1&post_type=post'); ?>

      <?php    if ($achievement->have_posts()) : ?>
        <div class="isotope">
      <?php while ($achievement->have_posts()) : $achievement->the_post();

      $thefield = get_field('achievement_type');
      ?>
   

      <div id="post-<?php the_ID(); ?>" <?php post_class( 'class2 element-item ' . $thefield . ' ' ); ?> >
        <h1 class="show-more"><?php the_title(); ?></h1>
        <h1 class="show-less"><?php the_titlesmall('', '...', true, '45') ?></h1>
          <div class="show-more"><?php the_content(); ?></div> 
          <div class="show-less"><?php the_excerpt(); ?></div>
          <div class="more">MORE</div>
      </div>


<?php    endwhile; ?>
</div>
<?php endif; 
  wp_reset_query();
?>


  
      <br class="float_clear"/>
    </div>

</div>
<?php
get_footer(); ?>
