<?php get_header(); ?>
<!-- Left content column -->
    <div id="wideContent">
      <div id="breadcrumbs">
        <a href="http://marquette.edu/">Marquette.edu</a> //
        <?php if($post->post_parent != false): ?>
        <a href="<?php the_permalink(wp_get_post_parent_id()); ?>"><?php echo get_the_title(wp_get_post_parent_id()); ?></a> //
        <?php endif; ?>
      </div>
      <div id="pageName">
        <h1><?php the_title(); ?></h1>
      </div>

        <!-- Main story item -->
      <?php
      if ( have_posts() ) :
        // Start the Loop.
        while ( have_posts() ) : the_post();

          /*
           * Include the post format-specific template for the content. If you want to
           * use this in a child theme, then include a file called called content-___.php
           * (where ___ is the post format) and that will be used instead.
           */
         the_content();

         ?>
        <p>
          <input type="text" id="quicksearch" placeholder="Search">
        </p>
      <div id="filters" class="button-group filter-button-group">
        <button class="button" data-filter="*">Show All</button>
         <?php

         wp_list_categories(array(
            'title_li'      => __(''),
            'style'         => 'none',
            'exclude'       => '1,4,11,12,13',
            'walker'        => new CatWalk
          )
        );

        ?>
      </div>
      <?php
        endwhile;
      endif;
      ?>


      <?php $preproposals = new WP_Query('posts_per_page=-1&post_type=sif-pre-proposal'); ?>

<?php    if ($preproposals->have_posts()) : ?>
  <div class="isotope">
<?php while ($preproposals->have_posts()) : $preproposals->the_post();
?>
      <div id="post-<?php the_ID(); ?>" <?php post_class('element-item'); ?>>
        <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
        <p><strong>Team contact:</strong> <?php the_field('contact-name'); ?><br />
          <a href="mailto:<?php the_field('contact-email'); ?>"><?php the_field('contact-email'); ?></a></p>
          <?php the_content(); ?>
          <div class="more">MORE</div>
      </div>


<?php    endwhile; ?>
</div>
<?php endif; 
  wp_reset_query();
?>


  
      <br class="float_clear"/>
    </div>

</div>
<?php
get_footer(); ?>
