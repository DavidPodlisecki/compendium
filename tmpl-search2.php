<?php
/**
 * Template Name: Search2
 * Description: Custom search template
 *
 * @package WordPress
 * @subpackage MU-v2
 * @since MU 1.0
 */

get_header(); ?>

<!-- Left content column -->
    <div id="wideContent">
      <div id="breadcrumbs">
        <p><a href="http://marquette.edu/">Marquette.edu</a> // <a href="<?php echo get_site_url(); ?>">Research and Scholarship</a> //
        <?php if($post->post_parent != false): ?>
        <a href="<?php the_permalink(wp_get_post_parent_id()); ?>"><?php echo get_the_title(wp_get_post_parent_id()); ?></a> //
        <?php endif; ?>
        </p>
      </div>
      <div id="pageName">
        <h1><?php the_title(); ?></h1>

      </div>
       <h1>Research and scholarship activity at Marquette</h1>
    
    
        <!-- Main story item -->
      <?php
      if ( have_posts() ) :
        // Start the Loop.
        while ( have_posts() ) : the_post();

          /*
           * Include the post format-specific template for the content. If you want to
           * use this in a child theme, then include a file called called content-___.php
           * (where ___ is the post format) and that will be used instead.
           */
         the_content();

         ?>
         

         
        <p>
          <input type="text" id="quicksearch" placeholder="Search">
          <!-- <button onclick="doSearch()" style="left-margin: 10px;">Search</button>
          <button onclick="doClear()" style="left-margin: 10px;">Clear</button> -->
        </p>
      <div id="filters" class="button-group filter-button-group">
       
         
        <div class="filter-wrp grid">
      <div class="blog-filter grid-item">
      <label>College or Department</label>
 
 <?php $wpdcargs = array(

	'show_option_none'   => 'All',
	'option_none_value'  => '*',
	'orderby'            => 'NAME', 
	'order'              => 'ASC',
	'show_count'         => 0,
	'hide_empty'         => 0, 
	'child_of'           => 0,
	'exclude'			       => '1',
	'echo'               => 1,
	'selected'           => 0,
	'hierarchical'       => 1, 
	'name'               => 'college',
	'id'                 => 'college',
	'class'              => 'categories',
	'depth'              => 0,
	'tab_index'          => 0,
	'taxonomy'           => 'category',
	'hide_if_empty'      => false,
	'value_field'	     => 'slug',	 
); ?>
 
       
	<?php wp_dropdown_categories( $wpdcargs ); ?>
      </div>  

      <!-- <div class="blog-filter grid-item">
        <label>Date</label>
        <select name="archive-dropdown" id="archive-dropdown">
  <option value="*"><?php echo esc_attr( __( '*' ) ); ?></option>  
  <?php wp_get_archives( array( 'type' => 'monthly', 'style'  => 'none', 'format' => 'option') ); ?>
</select>
      </div> -->

      <div class="blog-filter grid-item">
        <label>Achievement</label>
        <select name="achievement" id="achievementpulldown" class="">
          <option value="*">All</option>
          <option value="Appointed">Appointed</option>
          <option value="Awarded">Awarded</option>
          <option value="Chaired">Chaired</option>          
          <option value="Consulted">Consulted</option>
          <option value="Edited">Edited</option>          
          <option value="Elected">Elected</option>
          <option value="Grant">Grant</option>
          <option value="Inducted">Inducted</option>
          <option value="Organized">Organized</option>
          <option value="Patents">Patents</option>
          <option value="Presented">Presented</option>
          <option value="Published-Article">Published Article</option>
          <option value="Published-Book">Published Book</option>
          <option value="Published-Chapter">Published Chapter</option>
          <option value="Reviewed">Reviewed</option>
          <option value="Served">Served</option>
          <option value="Translated-Article">Translated Article</option>
          <option value="Translated-Book">Translated Book</option>
          <option value="Translated-Chapter">Translated Chapter</option>      
        </select>
      </div>  
    </div>
      </div>
      <div style="clear:both;"></div>
      <button onclick="filterAndSearch()" style="left-margin: 10px;">Search and Filter</button>
      <?php
        endwhile;
      endif;
      ?>

  <div id="noresults" class="message" style="padding: 15px 0;"><h3 style="text-align:center;">No results</h3></div>
	<div id="loading" class="message" style="padding: 15px 0;">
    <h3 style="text-align:center;">Loading results</h3>
    <img src="<?php echo get_template_directory_uri(); ?>/images/mu-loader.gif" style="    margin: 0 auto; display: block;" />
  </div>
	
  <div id="content" class="grid"> </div>
      <br class="float_clear"/>
  </div>
  <button onclick="nextPage()" id="nextPage">Load more</button>

  <style>
    #content {
      position: relative;
    }

    #nextPage {
      margin-top: 20px;
      margin-left: 10px;
    }

    .achievement {
        background: #f5f5f5;
        border: 1px solid #dcdcdc;
        border-left: 5px solid #003366;
    }

    .grid-item {
        float: left;
        height: 320px;
        width: 320px;
        margin: 5px 5px 0 0;
    }

    .name {
      line-height: 22px;
      font-size: 18px;
      color: #336699;
      width: 170px;
      min-height: 50px;
      padding: 15px 0 0 10px;
    }

    .right {
      position: relative;
      color: #666666;
      line-height: 160%;
      padding: 5px 0;
      font-size: 70%;
      opacity: 0.4;
      top: -60px;
      right: 10px;
      float: right;
    }

    .message {
      display: none;
    }

    .title {
      font-size: 16px;
      font-weight: normal;
      line-height: normal;
      text-transform: none;
      padding-top: 15px;
      border-top: 1px solid #ebebeb;
      margin: 0 10px 10px 10px;
    }

    .descr {
      color: #666666;
      font-size: 14px;
      line-height: 160%;
      padding: 5px 0;
      margin: 0 10px 10px 10px;
    }

    .blog-filter {
      height: 50px;
    }

    .blog-filter select {
      width: 100%;
    }
  </style>

    <script>

      var page = 1;
      var lastAddOn = "";
      var lastSearch = "";
      var lastCall = "";

      function clearSearchBox() {
        $("#quicksearch").val("");
      }

      function resetFilters() {
        // $("#college").val(0);
        document.getElementById("college").selectedIndex = 0;
        document.getElementById("achievementpulldown").selectedIndex = 0;
      }

      function showLoading(){
        hideNoResults();
        // $("#loading").css("display", "block");
        $("#nextPage").css("display", "none");
        $("#loading").slideDown();
      }

      function hideLoading() {
        // $("#loading").css("display", "none");
        $("#nextPage").css("display", "block");
        $("#loading").slideUp();
      }

      function showNoResults() {
        hideLoading();
        $("#noresults").css("display", "block");
        $("#nextPage").css("display", "none");
      }

      function hideNoResults() {
        $("#noresults").css("display", "none");
        $("#nextPage").css("display", "block");
      }

      $( document ).ready(function() {
          init();
      });

      function init() {
        getData({
          urlAddOn: "", 
          reset: false, 
          search: ""
        });
        //$("#college").change(collegeChange);
        //$("#achievementpulldown").change(achievementChange);
        $("#quicksearch").keyup(function(event){
            if(event.keyCode == 13){
                doSearch();
            }
        });
      }

      function collegeChange(ev) {
        page = 1; 
        document.getElementById("achievementpulldown").selectedIndex = 0;
        var target = $("#college").val();
        if (target !== "*") {
          getData({
            urlAddOn: "&filter[category_name]=" + target, 
            reset: true,
            search: lastSearch
          });
        } else {
          getData({
            urlAddOn: "", 
            reset: false, 
            search: ""
          });
        }
      }

      function achievementChange(ev) {
        page = 1;
        document.getElementById("college").selectedIndex = 0;
        clearSearchBox();
        var target = $("#achievementpulldown").val();
        clearResults();
        hideNoResults();
        showLoading();  
        if (target !== "*") {
          // $.getJSON( "<?php bloginfo('wpurl') ?>/wp-json/wp/v2/achievement" + url, function( data ) {
          var url = "<?php bloginfo('wpurl') ?>/wp-json/swp_api/search?meta_query[key]=achievement_type&meta_query[value]=" + target + "&meta_query[compare]=IN";
          $.getJSON(url, function( data ) {
              processResults(data, {
                reset: true,
                urlAddOn: ""
              });
          });
          lastCall = url;
        } else {
          getData({
            urlAddOn: "", 
            reset: false, 
            search: ""
          });
        }
        //     /wp-json/swp_api/search?meta_query[key]=achievement_type&meta_query[value]=Served&meta_query[compare]=IN
      }

      function nextPage() {
        page = page + 1;
        var url = lastCall.replace(/&page=[0-9]*/, "");
        url = url + "&page=" + page;
        $.getJSON(url, function( data ) {
            processResults(data, {
              reset: false,
              urlAddOn: lastAddOn,
              nextPage: true
            });
        });
      }

      function doSearch() {
        var target = $("#quicksearch").val();
        resetFilters();
        clearResults();
        hideNoResults();
        showLoading();  
        // $.getJSON( "<?php bloginfo('wpurl') ?>/wp-json/wp/v2/achievement" + url, function( data ) {
        var url = "<?php bloginfo('wpurl') ?>/wp-json/swp_api/search?s=" + target;
        $.getJSON(url, function( data ) {
            processResults(data, {
              reset: true,
              urlAddOn: ""
            });
        });
        lastCall = url;
      }

      function filterAndSearch() {
        showLoading();  
        var search = $("#quicksearch").val();
        var achievement = $("#achievementpulldown").val();
        var college = $("#college").val();
        var url = "<?php bloginfo('wpurl') ?>/wp-json/swp_api/search?posts_per_page=51&page=1";
        if (search.length) {
          url = url + "&s=" + search;
        }
        if (college !== "*") {
          url = url + "&tax_query[field]=name&tax_query[taxonomy]=category&tax_query[terms]=" + college;
        }
        if (achievement !== "*") {
          url = url + "&meta_query[key]=achievement_type&meta_query[value]=" + achievement + "&meta_query[compare]=IN";
        }
        $.getJSON(url, function( data ) {
            processResults(data, {
              reset: true,
              urlAddOn: ""
            });
        });
        lastCall = url;
      }

      function doClear() {
        $("#quicksearch").val("");
        getData({
          urlAddOn: lastAddOn, 
          reset: true,
          search: ""
        });
      }

      function clearResults() {
        $("#content").html("");
      }

      function getData(options) {
        clearSearchBox();
        clearResults();
        hideNoResults();
        showLoading();   
        var urlBase = "?per_page=51&page=" + page;
        // var urlBase = "?per_page=5";
        var url = urlBase + options.urlAddOn;
        // if (options.search.length) {
        //   url = url + "&search=" + options.search;
        // }
        options.reset = options.reset || false;
        url = "<?php bloginfo('wpurl') ?>/wp-json/wp/v2/achievement" + url;
        $.getJSON( url, function( data ) {
        // $.getJSON( "<?php bloginfo('wpurl') ?>/wp-json/swp_api/search?s=jennifer" + url, function( data ) {
            processResults(data, options);
        });
        lastCall = url;
      }

      function processResults(data, options) {
        console.log("data", data);
        var allHtml = "";
        for (var i = 0; i < data.length; i++) {
          var name = data[i].author_profile.data.display_name;
          var title = data[i].author_acf.title;
          var datetime = "2016"; //temp
          var achievement_type = data[i].achievement_type;
          var descr = achievement_type + ": " + data[i].title.rendered;

          var html = '<div class="achievement grid-item">'
             + '<div class="row">'
               + '<div class="name">' + name + '</div>'
               + '<div class="right">' + datetime + "<br/>" + achievement_type + '</div>'
             + '</div>'
             + '<div class="row">'
               + '<div class="title">' + title + '</div>'
             + '</div>'
             + '<div class="row">'
               + '<div class="descr">' + descr + '</div>'
             + '</div>'
           + '</div>';
           allHtml = allHtml + html;
        }
        if (data.length) {
          if (options.reset) {
            page = 1;
            $("#content").css("display", "none");
            $("#content").html(allHtml);
            $("#content").fadeIn(1000);
          } else {
            var newClass = "appended-" + Date.now();
            $("#content").append("<span style='display: none;' class='"+newClass+"'>" + allHtml + "</span>");
            $("." + newClass).slideDown(1500);
          }
          
          hideLoading();
        } else {
          if (options.hasOwnProperty("nextPage")) {
            $("#nextPage").css("display", "none");
          } else {
            clearResults();
            showNoResults();
          }
        }
        lastAddOn = options.urlAddOn; 
      }
    </script>
</div>
<?php
get_footer(); ?>
