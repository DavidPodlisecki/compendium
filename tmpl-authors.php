<?php
/**
 * Template Name: Faculty
 * Description: Custom search template
 *
 * @package WordPress
 * @subpackage MU-v2
 * @since MU 1.0
 */

get_header(); ?>
<!-- Left content column -->
    <div id="wideContent">

       <div class="facultyFilter">
      
          <input type="text" id="quicksearch" placeholder="Search">
        
      
  

 <?php $wpdcargs = array(

	'show_option_none'   => 'View faculty by college',
	'option_none_value'  => '*',
	'orderby'            => 'NAME', 
	'order'              => 'ASC',
	'show_count'         => 0,
	'hide_empty'         => 0, 
	'child_of'           => 0,
	'exclude'			 => '1,4,11,12,13, 49',
	'echo'               => 1,
	'selected'           => 0,
	'hierarchical'       => 1, 
	'name'               => 'college',
	'id'                 => 'college',
	'class'              => 'categories',
	'depth'              => 0,
	'tab_index'          => 0,
	'taxonomy'           => 'category',
	'hide_if_empty'      => false,
	'value_field'	     => 'slug',	
); ?>
 
       
	<?php wp_dropdown_categories( $wpdcargs ); ?>


      </div>  
      </div>
   
      <div id="breadcrumbs">
        <a href="http://marquette.edu/">Marquette.edu</a> //
        <?php if($post->post_parent != false): ?>
        <a href="<?php the_permalink(wp_get_post_parent_id()); ?>"><?php echo get_the_title(wp_get_post_parent_id()); ?></a> //
        <?php endif; ?>
      </div>
      <div id="pageName">
        <h1><?php the_title(); ?></h1>

      </div>
            

<?php
      if ( have_posts() ) :
        // Start the Loop.
        while ( have_posts() ) : the_post();

          /*
           * Include the post format-specific template for the content. If you want to
           * use this in a child theme, then include a file called called content-___.php
           * (where ___ is the post format) and that will be used instead.
           */
         the_content();

         ?>
     
        <div class="isotope">
      
      </div>

      <?php
        endwhile;
      endif;
      ?>
      


  
      <br class="float_clear"/>
    </div>
</div>
</div>
<?php
get_footer(); ?>
