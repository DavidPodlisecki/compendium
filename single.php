<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<!-- Left content column -->
    <div id="leftContent">
      <div id="pageIdentity"> 
        <!-- Breadcrumbs -->
        <div id="breadcrumbs">
          <p><a href="http://www.marquette.edu">Marquette.edu</a> //  <a href="<?php echo get_site_url(); ?>">Research and Scholarship</a> // </p>
        </div>
         <!-- Page name -->
        <div id="pageName">
             <?php while ( have_posts() ) : the_post();  ?>
     		  <h1><?php the_title();?></h1>
     </div>
    </div>
	 <?php the_content();?>
      
      
	<?php endwhile ?>
    <?php previous_post('&laquo; &laquo; %', '', 'yes'); ?>
| <?php next_post('% &raquo; &raquo; ', '', 'yes'); ?>
    </div> 
       
    <!-- End left content --> 
    
    <!-- Start sidebar content -->
    <div id="sidebarRightImage">
      <div id="pageImage">
     
      <?php if ( has_post_thumbnail() ) { the_post_thumbnail('single_page-thumb'); 
	  		} else { ?>
	  		<img src="<?php bloginfo('template_directory'); ?>/images/ft-img-placeholder.jpg"/>
	  <?php } ?>  
      </div>
      <div id="sidebarRight">
        <div id="columnHeader">
          <h1> Quick links</h1>
        </div>
        <div id="content">
          <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Page Sidebar') ) : ?><?php endif; ?>
        </div>
      </div>
      <br class="float_clear" />
    </div>
    <!-- End right sidebar --> 
    <br class="float_clear"/>
  </div>

</div>
<?php
get_footer();
