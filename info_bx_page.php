<?php

/**

 * Template Name: Info Box

 * Description: Page with infomation box

 **/


get_header(); ?>

<!-- Left content column -->
    <div id="leftContent">
      <div id="pageIdentity"> 
        <!-- Breadcrumbs -->
        <div id="breadcrumbs">
          <p><a href="http://www.marquette.edu">Marquette.edu</a> //  <a href="<?php echo get_site_url(); ?>">Research and Scholarship</a> // </p>
        </div>
        
        <!-- Page name -->
        <div id="pageName">
         
             <?php while ( have_posts() ) : the_post();  ?>
     <h1><?php the_title();?></h1>
     </div>
    </div>
	 <?php the_content();?>
    <div id="leftInformation">
        <h1><?php $info_title = get_post_meta($post->ID, 'wpcf-title', true); echo $info_title;?></h1>
        <?php $info_content = get_post_meta($post->ID, 'wpcf-extra-info', true); echo $info_content; ?>
      </div>
	<?php endwhile ?>
    </div>
    <!-- 
    
    -->
    <!-- End left content --> 
    
    <!-- Start sidebar content -->
    <div id="sidebarRightImage">
      <div id="pageImage">
      <?php the_post_thumbnail( 'single_page-thumb' ); ?> 
      </div>
      <div id="sidebarRight">
        <div id="columnHeader">
          <h1> Quick links</h1>
        </div>
        <div id="content">
          <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Page Sidebar') ) : ?><?php endif; ?>
        </div>
      </div>
      <br class="float_clear" />
    </div>
    <!-- End right sidebar --> 
    <br class="float_clear"/>
  </div>

</div>
<?php
get_footer();
