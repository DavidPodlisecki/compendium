<?php get_header(); ?>


<?php 
	$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author)); 
	$acfuserid = 'user_' . intval($author);
?>

<div id="content" class="narrowcolumn">
    <!-- Left content column -->
    <div id="leftContent">    
      <div id="pageIdentity"> 
        <!-- Breadcrumbs -->
        <!-- <div id="breadcrumbs">
                   <p>   <a href="http://www.marquette.edu">Marquette.edu</a> // <a href="/">Research & Scholarship</a> // <a href="index-main.php">Compendium</a> // <a href="faculty.php">Faculty</a></p>


        </div> -->
        
        <!-- Page name -->
        <div id="pageName">
          <h1><?php the_field('prefix', $acfuserid); ?> <?php the_author_meta( 'display_name', $author ); ?></h1>
          <h2><em><?php the_field('title', $acfuserid); ?></em></h2>
        </div>
       
      </div>
    
 
    <div style="float:left;"><?php $image1 = get_field('photo' , $acfuserid);  if( !empty($image1) ): ?><img src="<?php echo $image1['url']; ?>" border="0" alt="<?php echo $image1['alt']; ?>" align="left" class="profilePhoto" width="220" height="220" /><?php endif ;?>
   <!-- <p><img src="/wp-content/themes/compendium/images/phone-icon.gif" alt="Phone:" border="0" class="icon"><?php the_field('phone', $acfuserid); ?></p>-->
    <p><img src="/wp-content/themes/compendium/images/email-icon.gif" alt="Email:" border="0" class="icon"><a href="mailto:<?php the_author_meta( 'user_email', $author ); ?>"><?php the_author_meta( 'user_email', $author ); ?></a>  </p>
    <!-- <p> <img src="/wp-content/themes/compendium/images/pdf-icon.gif" alt="CV:" border="0" class="icon"> <a href="<?php the_field('curriculum', $acfuserid); ?>"><?php the_field('prefix', $acfuserid); ?> <?php the_author_meta( 'last_name', $author ); ?>'s Curriculum Vitae</a></p> -->
    </div>
      
     <p class="intro" style="margin-top:-20px;"><?php the_author_meta( 'user_description', $author ); ?></p>
     <div style="clear:both;"></div>
    

<!-- This sets the $curauth variable -->


  
           <style>
           .accord-content { display: none; }
           ul.no-style li{list-style: none !important; }
          </style>
          <?php
            $author = get_user_by( 'slug', get_query_var( 'author_name' ) );
            query_posts(array('author' => $author->ID , 'nopaging' => 1,));
            $prev_year = null;
              if ( have_posts() ) {
                echo '<div class="accordion">';
                while ( have_posts() ) {
                  the_post();
                  $this_year = get_the_date('Y');
                  if ($prev_year != $this_year) {
                    // Year boundary
                    if (!is_null($prev_year)) {
                    // A list is already open, close it first
                      echo '</ul>';
                      echo '</div>';
                    }
                    echo '<h3 class="accord-header"><a class="nav-toggle show-class">' . $this_year . '</a></h3>';
                    echo '<div class="accord-content">';
                    echo '<ul class="no-style">';
                  }
                 echo '<li>';
                 echo'<h2>';
                 the_field('achievement_type');
                 echo'</h2>' ;
                 the_title();
                 the_content();
                 echo '</li>';
                 $prev_year = $this_year;
              }
            echo '</div>';
            echo '</ul>';
            echo '</div>';
          }?>

 		 
    </div>
    <!-- End left content --> 
    
     <!-- Start sidebar content -->
     <?php  $cat = get_user_meta($author->ID,'_author_cat',true);

  if (empty($cat) || count($cat) <= 0 || !is_array($cat))
  {
    echo "none";
  }
  else
  { ?>
    <div id="sidebarRightImage">
      
      <?php $catImg = get_field('school_image', 'category_' . $cat[0] . '' ); ?>
   
      <div id="mainImage"> <img src="<?php  echo $catImg; ?>" /></div>
      <div id="sidebarRight">
<div id="columnHeader">
          <h1><?php echo get_cat_name( $cat[1] ); ?></h1>
        </div>
        <div id="content">
          <?php
		echo "<h2>";
		echo get_cat_name( $cat[0] );
		echo "</h2>"; 
    echo "<p>";
    echo category_description( $cat[0] ); 
    echo "</p>";
    echo "<br>";

    echo  "<div class='no-resize-img'>";

    if( get_field('twitter', 'category_' . $cat[0] . '' ) ): ?>
        <a href=" <?php the_field('twitter', 'category_' . $cat[0] . '' ); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter-bw.png" class="social"></a>
    <?php endif;
    if( get_field('facebook', 'category_' . $cat[0] . '' ) ): ?>
       <a href="<?php the_field('facebook', 'category_' . $cat[0] . '' ); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/facebook-bw.png" class="social"></a> 
    <?php endif;
    echo "</div>";
    ;} ?>
        <p>

       
</div>
<div style="clear:both; padding-top:20px;"></div>
        </div>
      </div>
      <br class="float_clear" />
    </div>
    <!-- End right sidebar --> 
    <br class="float_clear"/>

  </div>
</div>

<script type="text/javascript"> 
$(document).ready(function () {

     //$( ".accordion .accord-content" ).first().show();
     $( ".accordion .accord-content" ).first().show();
     $( " a.nav-toggle" ).first().addClass('hide-class').removeClass('show-class');

     
     $(".accordion .accord-header").click(function() {
      if($(this).next("div").is(":visible")){
        $(this).next("div").slideUp("slow");
         $('a.nav-toggle').each(function() {
          $(this).addClass('show-class').removeClass('hide-class');
        });
        //$(this).find('a.nav-toggle').addClass('hide-class').removeClass('show-class');
        console.log("active");
      } else {
        $(".accordion .accord-content").slideUp("fast");
        $(this).next("div").slideToggle("fast");
       // $(this).find('a.nav-toggle').addClass('show-class').removeClass('hide-class');
        $('a.nav-toggle').each(function() {
          $(this).addClass('show-class').removeClass('hide-class');
        });
        $(this).find('a.nav-toggle').addClass('hide-class').removeClass('show-class');
        
      }
    });
  });
</script>

<?php get_footer(); ?>