<?php
/**
 * The template for displaying a "No posts found" message
 *
 * @package WordPress
 * @subpackage MU
 * @since MU 1.0
 */
?>

        <p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>