[<?php

if (have_posts()) : 
  $counter = 0;
	while (have_posts()) : the_post();

		$thefield = get_field('achievement_type');
		$thedate = get_the_date('M d Y');
		$tagdate = get_the_date('FY');
		$start_year = get_field('start_year');
		$end_year= get_field('end_year');
		$current_year = date("Y");
		$start_month =get_field('start_month');
		$end_month = get_field('end_month');



	if (($thefield=='Reviewed') ||  ($thefield=='Edited') ||  ($thefield=='Elected') ||  ($thefield=='Appointed') ||  ($thefield=='Served') ||  ($thefield=='Organized'))
	{
	
		if ($start_year == "")  
		{
			if ($end_year == "")  
			{
				$start_year = $current_year;
				$end_year = $current_year;
				
			} else { $start_year = $end_year; } 
		}
	
		if ($end_year == "")  
		{
			$end_year = $start_year;
		}
	
	
		if ($start_month == "") 
		{
			$start_month = "January";
			
			if ($end_month =="")
			{		
				$end_month = "December";
			}
		}
		  

		if ($end_month =="")
		{		
			if ($start_month == "January")
			{	
				$end_month = "December";
		
			} else {$end_month = $start_month;}
		}

		if ($start_year != $end_year)  
		{			
 			$thedate = $start_month . " " . $start_year . " - " . $end_month . " " . $end_year; 
 		}
 		else 
 		{
 			if ($end_month != $start_month) {
 				if (($start_month == "January") && ($end_month == "December") ) {
 				$thedate = $start_year;
 				}else {  $thedate = $start_month . " " . $start_year . " - " . $end_month . " " . $end_year;  }
 			} else {$thedate = $start_month ." " . $end_year; }
 		}
 		//determine months to tag
 		

 
 


$el_start_month = explode(" ", $start_month);
$el_end_month = explode(" ", $end_month);

   $begin = DateTime::createFromFormat('j-M-Y', '1-' . $el_start_month[0] .'-'. $start_year);
	

    $end = DateTime::createFromFormat('j-M-Y', '28-' . $el_end_month[0] .'-'. $end_year);

		
		
		$interval = new DateInterval('P1M');
		$daterange = new DatePeriod($begin, $interval ,$end);
		
		$tagdate = '';
		foreach($daterange as $date){
			$tagdate .= $date->format("MdY") . " ";
		}
 
 		
	}
    $jsonClass = $thefield . ' ' . $tagdate;
    $cat = get_the_author_meta('_author_cat');
    if (empty($cat) || count($cat) <= 0 || !is_array($cat)) {}
    else { 
        $jsonClass  = $jsonClass . ' category-' . get_cat_slug($cat[0] ) . ' category-' . get_cat_slug($cat[1]); 
    }
    $author_id = get_the_author_meta('ID');
    $author_title = get_field('title', 'user_'. $author_id );

      $bio = "";
      
      if (get_field('achievement_type')=='Grant') {
         $bio = "Grant: " . get_field('grant_amount') . " " . the_title('', '', FALSE) . " " . get_field('awarding_organization'); 
          if (get_field('collaborators')!='') {
            $bio = $bio . " " . get_field('collaborators'); 
          } 
      } elseif (get_field('achievement_type')=='Published-Book') {
          $bio = "";
          if (get_field('first_author')!='') {
            $bio = $bio . "First Author: " . get_field('first_author');
          }
          $bio = $bio . "Published Book: " . the_title('', '', FALSE);
          if (get_field('collaborators')!='') { 
            $bio = $bio . get_field('collaborators'); 
          } 
      } elseif (get_field('achievement_type')=='Published-Chapter') {
          $bio = "";
          if (get_field('first_author')!='') {
            $bio = $bio . "First Author: " . get_field('first_author');
          }
          $bio = $bio . "Published Chapter: " . the_title('', '', FALSE);
          if (get_field('collaborators')!='') { 
            $bio = $bio . get_field('collaborators'); 
          } 
      } elseif (get_field('achievement_type')=='Published-Article') {
          $bio = "";
          if (get_field('first_author')!='') {
            $bio = $bio . "First Author: " . get_field('first_author');
          }
          $bio = $bio . "Published Article: " . the_title('', '', FALSE);
          if (get_field('collaborators')!='') { 
            $bio = $bio . get_field('collaborators'); 
          }  
      } elseif (get_field('achievement_type')=='Translated-Book') {
          $bio = "";
          if (get_field('first_author')!='') {
            $bio = $bio . "First Author: " . get_field('first_author');
          }
          $bio = $bio . "Translated Book: " . the_title('', '', FALSE);
          if (get_field('collaborators')!='') { 
            $bio = $bio . get_field('collaborators'); 
          }  
      } elseif (get_field('achievement_type')=='Translated-Chapter') {
          $bio = "";
          if (get_field('first_author')!='') {
            $bio = $bio . "First Author: " . get_field('first_author');
          }
          $bio = $bio . "Translated Chapter: " . the_title('', '', FALSE);
          if (get_field('collaborators')!='') { 
            $bio = $bio . get_field('collaborators'); 
          }  
      } elseif (get_field('achievement_type')=='Translated-Article') {
          $bio = "";
          if (get_field('first_author')!='') {
            $bio = $bio . "First Author: " . get_field('first_author');
          }
          $bio = $bio . "Translated Article: " . the_title('', '', FALSE);
          if (get_field('collaborators')!='') { 
            $bio = $bio . get_field('collaborators'); 
          }  
      } elseif (get_field('achievement_type')=='Reviewed') {
          $bio = "Reviewed: " . the_title('', '', FALSE);
          if (get_field('collaborators')!='') {
            $bio = $bio . get_field('collaborators');
          } else { 
            $bio = $bio . ".";
          } 
      } elseif (get_field('achievement_type')=='Edited') {
          $bio = "Edited: " . the_title('', '', FALSE);
          if (get_field('collaborators')!='') {
            $bio = $bio . get_field('collaborators');
          } else { 
            $bio = $bio . ".";
          } 
      } elseif (get_field('achievement_type')=='Presented') {
          $bio = "Presented: " . the_title('', '', FALSE) . ", " . get_the_date('F, Y') . " ";
          if (get_field('collaborators')!='') {
            $bio = $bio . get_field('collaborators');
          } else { 
            $bio = $bio . ".";
          } 
      } elseif (get_field('achievement_type')=='Chaired') {
          $bio = "Chaired: " . the_title('', '', FALSE);
          if (get_field('collaborators')!='') { 
            $bio = $bio . get_field('collaborators') . " " . get_the_date('F, Y');
          }
      } elseif (get_field('achievement_type')=='Awarded') {
          $bio = "Awarded: " . the_title('', '', FALSE) . " " .get_field('awarding_organization') . " " . get_the_date('Y');
          if (get_field('collaborators')!='') {
            $bio = $bio . get_field('collaborators');
          } else { 
            $bio = $bio . ".";
          }
      } elseif (get_field('achievement_type')=='Elected') {
          $bio = "Elected: " . the_title('', '', FALSE) . " " . $thedate;
          if (get_field('collaborators')!='') {
            $bio = $bio . get_field('collaborators');
          } else { 
            $bio = $bio . ".";
          }
      } elseif (get_field('achievement_type')=='Appointed') {
          $bio = "Appointed: " . the_title('', '', FALSE) . " " . $thedate;
          if (get_field('collaborators')!='') {
            $bio = $bio . get_field('collaborators');
          } else { 
            $bio = $bio . ".";
          }
      } elseif (get_field('achievement_type')=='Inducted') {
          $bio = "Inducted: " . the_title('', '', FALSE) . " " . get_the_date('F, Y') . " "; 
          if (get_field('collaborators')!='') {
            $bio = $bio . get_field('collaborators');
          } else { 
            $bio = $bio . ".";
          }
      } elseif (get_field('achievement_type')=='Served') {
          $bio = "Served: " . the_title('', '', FALSE) . " " . $thedate;
      } elseif (get_field('achievement_type')=='Consulted') {
          $bio = "Consulted: " . the_title('', '', FALSE) . " " . get_the_date('F, Y') . " ";
          if (get_field('collaborators')!='') {
            $bio = $bio . get_field('collaborators');
          } else { 
            $bio = $bio . ".";
          }
      } elseif (get_field('achievement_type')=='Organized') {
          $bio = "Organized: " . the_title('', '', FALSE) . " " . $thedate;
          if (get_field('collaborators')!='') {
            $bio = $bio . get_field('collaborators');
          } else { 
            $bio = $bio . ".";
          }
      } elseif (get_field('achievement_type')=='Patents') {
          $bio = "Patents:" . the_title('', '', FALSE); 
          if (get_field('collaborators')!='') {
            $bio = $bio . get_field('collaborators');
          } else { 
            $bio = $bio . ".";
          }
      } else { 
          $bio = "the_title('', '', FALSE); "; 
      } 
      if ($counter != 0) {
        echo ",";
      }
      ?>
{"id": "<?php the_ID(); ?>", "author": <?php echo json_encode(get_the_author()); ?>, "class": <?php echo json_encode($jsonClass); ?>, "title": <?php echo json_encode($author_title) ?>, "date": <?php echo json_encode($thedate); ?>, "activity": <?php echo json_encode(get_field('achievement_type')); ?>, "bio": <?php echo json_encode($bio) ?>}  
<?php
  $counter = $counter + 1;
	endwhile;
endif; 
?>
]
