<?php
/**
 * The Header forget_field our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage MU
 * @since MU 1.0
 */

/* disable single posts view for acheivements */
if( is_single() ) {
	wp_redirect( '/thispagedoesnotexist', 301 ); exit; 
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if lte IE 7]> <html class="ie7"> <![endif]-->
<!--[if IE 8]>     <html class="ie8"> <![endif]-->
<!--[if IE 9]>     <html class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html>
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<!-- Description about this page -->
<meta name="description" content="Landing A1 website template for the 2013 Marquette.edu design." />
<!-- Title of page -->
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/univers-edu.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/top-navigation.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/landing-right-sidebar.css" type="text/css" media="screen, print" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/footer.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/secondary-b.css" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/responsive-tabs.css">
<meta name="viewport" content="width=device-width">
<!-- IE6-8 support of HTML5 elements --><!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<script type="text/javascript" src="//code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/edu-menu-jquery.js"></script>
<?php if(is_page_template('tmpl-custom.php')): ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/js/jquery.bxslider.css" />
<script language="javascript" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.bxslider.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.infinitescroll.js"></script>

<?php elseif(is_page_template('tmpl-search.php') || is_page_template('tmpl-authors.php')): ?>
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/sif-search.css" type="text/css" media="screen" />
<!-- <script language="javascript" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/isotope.pkgd.min.js"></script> -->
<script language="javascript" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/isotope.pkgd.3.0.1.js"></script> 
<!-- <script language="javascript" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.isotope.min.1.5.26.js"></script> -->
<script language="javascript" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/masonry.pkgd.min.js"></script> 
<script language="javascript" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/easy-responsive-tabs.js"></script>
<?php endif; ?> 
<script language="javascript" type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/easy-responsive-tabs.js"></script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1023600-16']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script type="text/javascript"> 
$(document).ready(function () {
$('.responsive-tabs').easyResponsiveTabs({
type: 'default', //Types: default, vertical, accordion
width: 'auto', //auto or any width like 600px
fit: true, // 100% fit in a container
closed: 'accordion',
});
        $(".resp-tab-item:nth-child(1)").click(); //use this to link to a specific tab
});
</script> 

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<!-- Start page container -->
<div id="container"> 
    <!-- Top navigation area -->
<div id="goldBar"></div>
<div id="topNavigation">
<div id="topTools">
    <ul>
      <li id="homepageLink"><a href="http://www.marquette.edu">marquette.edu</a> // </li>
      <li id="searchLink"><a href="/search/">Search</a> // </li>
      <li id="contactsLink"><a href="/tools/campus-contacts.php">Contacts</a> // </li>
      <li id="toolsLink"><a href="/tools/atoz.php">A-Z Index</a></li>
    </ul>
    
  </div>
  <!-- include("../_global-includes/navigation-top-tools.php")  -->
  <div id="menuContainer">
    <div id="mobile-header">
      <h2><a href="<?php echo  get_theme_mod( 'MobileHeader_link' ); ?>"><?php echo  get_theme_mod( 'MobileHeader_title' ); ?></a></h2>
      <h1><a href="<?php echo  get_theme_mod( 'MobileSubHeader_link' ); ?>"><?php echo  get_theme_mod( 'MobileSubHeader_title' ); ?></a></h1>
    </div> 
  <div id="logo"><a href="http://www.marquette.edu"><img src="<?php bloginfo('template_directory'); ?>/images/marquette-logo-transparent-white.png" alt="Marquette University logo" /></a>
    <div id="siteTitle">
      <h1><a href="<?php echo get_bloginfo('url') ?>"><?php echo bloginfo('name'); ?></a></h1>
    </div>
  </div>
  <br class="float_clear" />
  <ul id="menu">
    <li id="navOne"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Nav Widget One') ) : ?><?php endif; ?></li>
    <li id="navTwo"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Nav Widget Two') ) : ?><?php endif; ?></li>
    <li id="navThree"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Nav Widget Three') ) : ?><?php endif; ?></li>
    <li id="navFour"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Nav Widget Four') ) : ?><?php endif; ?></li>
    <li id="navFive"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Nav Widget Five') ) : ?><?php endif; ?>
    </li>
    <li id="navSix"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Nav Widget Six') ) : ?><?php endif; ?></li>
    <li id="navSeven"><?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Nav Widget Seven') ) : ?><?php endif; ?></li>
  </ul> 
  </div>
</div>
<script type="text/javascript">
// Add search
$("#mobile-header h1").after('<div id="searchButton"><a href="/search/">Search ▸</a></div><div id="button">MENU &#9776;</div>');
// Toggle button
 $("#button").click(function(){
		$("ul#menu").toggleClass("activeMenu"); 
		$("#button").toggleClass("menuHighlight"); 
}); 
</script>

  <!-- Main content container -->
  <!--<div id="wrapperContent">
  <?php if( is_home() || is_front_page() ) : ?>
    <div id="mainImage"> <img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="" /> </div>
<?php endif; ?>-->
<div id="wrapperContent">
    <!-- Left content column -->

