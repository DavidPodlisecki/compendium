<?php
/**
 * Template Name: Search
 * Description: Custom search template
 *
 * @package WordPress
 * @subpackage MU-v2
 * @since MU 1.0
 */

get_header(); ?>
<!-- Left content column -->
    <div id="wideContent">
      <div id="breadcrumbs">
        <a href="http://marquette.edu/">Marquette.edu</a> //  <a href="<?php echo get_site_url(); ?>">Research and Scholarship</a> //
        <?php if($post->post_parent != false): ?>
        <a href="<?php the_permalink(wp_get_post_parent_id()); ?>"><?php echo get_the_title(wp_get_post_parent_id()); ?></a> //
        <?php endif; ?>
      </div>
      <div id="pageName">
        <h1><?php the_title(); ?></h1>

      </div>
       <h1>Research and scholarship activity at Marquette</h1>
      <p>Search by keyword, college or department. <br> Default sort is most recent. </p>
        <!-- Main story item -->
      <?php
      if ( have_posts() ) :
        // Start the Loop.
        while ( have_posts() ) : the_post();

          /*
           * Include the post format-specific template for the content. If you want to
           * use this in a child theme, then include a file called called content-___.php
           * (where ___ is the post format) and that will be used instead.
           */
         the_content();

         ?>
        <p>
          <input type="text" id="quicksearch" placeholder="Search">
        </p>
      <div id="filters" class="button-group filter-button-group">
       
         
        <div class="filter-wrp">
      <div class="blog-filter">
      <label>College or Department</label>
       <select name"college" id="college">
        <option value="*">All</option>
       <?php

         wp_list_categories(array(
            'title_li'      => __(''),
            'style'         => 'none',
            'exclude'       => '1,4,11,12,13',
            'walker'        => new CatWalk,
            'hide_empty'=> '0',
            'hierarchical' => '1'
          )
        );

        ?>
      </select>

      </div>  

      <div class="blog-filter">
        <label>Date</label>
        <select name="archive-dropdown" id="archive-dropdown">
  <option value="*"><?php echo esc_attr( __( '*' ) ); ?></option>  
  <?php wp_get_archives( array( 'type' => 'monthly', 'style'  => 'none', 'format' => 'option') ); ?>
</select>
      </div>

      <div class="blog-filter">
        <label>Achievement</label>
        <select name="achievement" id="achievementpulldown" class="">
          <option value="*">All</option>
          <option value=".Grant">Grant</option>
          <option value=".Published-Book">Published Book</option>
          <option value=".Published-Chapter">Published Chapter</option>
          <option value=".Published-Article">Published Article</option>
          <option value=".Translated-Book">Translated Book</option>
          <option value=".Translated-Chapter">Translated Chapter</option>
          <option value=".Translated-Article">Translated Article</option>
          <option value=".Reviewed">Reviewed</option>
          <option value=".Edited">Edited</option>
          <option value=".Presented">Presented</option>
          <option value=".Chaired">Chaired</option>
          <option value=".Awarded">Awarded</option>
          <option value=".Elected">Elected</option>
          <option value=".Appointed">Appointed</option>
          <option value=".Appointed">Appointed</option>
          <option value=".Inducted">Inducted</option>
          <option value=".Served">Served</option>
          <option value=".Consulted">Consulted</option>
          <option value=".Organized">Organized</option>
          <option value=".Patents">Patents</option>
        </select>
      </div>  
    </div>
      </div>
      <?php
        endwhile;
      endif;
      ?>


      <?php $achievement = new WP_Query('posts_per_page=-1&post_type=post'); ?>

      <?php    if ($achievement->have_posts()) : ?>
        <div class="isotope">
      <?php while ($achievement->have_posts()) : $achievement->the_post();

      $thefield = get_field('achievement_type');
      $thedate = get_the_date('FY');
      
      ?>
      

      <div id="post-<?php the_ID(); ?>" class="<?php echo 'class2 element-item ' . $thefield . ' ' . $thedate;      
          $cat = get_the_author_meta('_author_cat');
  if (empty($cat) || count($cat) <= 0 || !is_array($cat))
  {
  }
  else
  { 
  
	echo ' category-'; echo get_cat_slug($cat[0] );	echo ' category-'; echo get_cat_slug($cat[1] );	


  }?>">
        <!-- <h1 class="show-more"><?php the_title(); ?></h1>
        <h1 class="show-less"><?php the_titlesmall('', '...', true, '45') ?></h1> -->
        <h2 class="author"><?php the_author();?></h2>
        <?php
        $author_id = get_the_author_meta('ID');
        $author_title = get_field('title', 'user_'. $author_id );
        ?>

        
        <h3 class="author-title"><?php echo $author_title ?></h3>
        <div class="date-info">
          <p class="date"><?php echo get_the_date('F Y'); ?></p>
          <p class="activity"><?php the_field('achievement_type'); ?></p>
        </div> 
       
          <div class="show-less post-txt"><p><?php the_titlesmall('', '...', true, '45') ?></p></div>
          
          <?php if (get_field('achievement_type')=='Grant') {?>
             <div class="show-more post-txt">Grant: <?php the_field('grant_amount'); ?>, "<?php the_title(); ?>," <?php the_field('awarding_organization'); ?><?php if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div> 
          <?php } elseif (get_field('achievement_type')=='Published-Book') {?>
             <div class="show-more post-txt">First Author: <?php the_field('first_author'); ?>, Published Book"<?php the_title(); ?>, <?php echo get_the_date('Y'); ?> ," <?php if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div> 
          <?php } elseif (get_field('achievement_type')=='Published-Chapter') {?>
             <div class="show-more post-txt">First Author: <?php the_field('first_author'); ?>, Published Chapter:"<?php the_title(); ?>, <?php echo get_the_date('Y'); ?> ," <?php if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div>
          <?php } elseif (get_field('achievement_type')=='Published-Article') {?>
             <div class="show-more post-txt">First Author: <?php the_field('first_author'); ?>, Published Article:"<?php the_title(); ?>, <?php echo get_the_date('Y'); ?> ," <?php if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div> 
          <?php } elseif (get_field('achievement_type')=='Translated-Book') {?>
             <div class="show-more post-txt">First Author: <?php the_field('first_author'); ?>, Translated Book:"<?php the_title(); ?>, <?php echo get_the_date('Y'); ?> ," <?php if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div> 
          <?php } elseif (get_field('achievement_type')=='Translated-Chapter') {?>
             <div class="show-more post-txt">First Author: <?php the_field('first_author'); ?>, Translated Chapter:"<?php the_title(); ?>, <?php echo get_the_date('Y'); ?> ," <?php if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div> 
          <?php } elseif (get_field('achievement_type')=='Translated-Article') {?>
             <div class="show-more post-txt">First Author: <?php the_field('first_author'); ?>, Translated Article:"<?php the_title(); ?>, <?php echo get_the_date('Y'); ?> ," <?php if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div> 
          <?php } elseif (get_field('achievement_type')=='Reviewed') {?>
             <div class="show-more post-txt">Reviewed: "<?php the_title(); ?>," <?php if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div> 
          <?php } elseif (get_field('achievement_type')=='Edited') {?>
             <div class="show-more post-txt">Edited: "<?php the_title(); ?>," <?php if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div> 
          <?php } elseif (get_field('achievement_type')=='Presented') {?>
             <div class="show-more post-txt">Presented: "<?php the_title(); ?>, <?php echo get_the_date('F, Y'); ?> " <?php if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div> 
          <?php } elseif (get_field('achievement_type')=='Chaired') {?>
             <div class="show-more post-txt">Chaired: "<?php the_title(); ?>," <?php if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div>
          <?php } elseif (get_field('achievement_type')=='Awarded') {?>
             <div class="show-more post-txt">Awarded: "<?php the_title(); ?>", <?php the_field('awarding_organization'); ?>, <?php echo get_the_date('Y'); ?> ," <?php if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div>
          <?php } elseif (get_field('achievement_type')=='Elected') {?>
             <div class="show-more post-txt">Elected: "<?php the_title(); ?>, <?php echo get_the_date('F, Y'); ?> ," <?php if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div>
          <?php } elseif (get_field('achievement_type')=='Appointed') {?>
             <div class="show-more post-txt">Appointed: "<?php the_title(); ?>, <?php echo get_the_date('F, Y'); ?> ," <?php if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div>
          <?php } elseif (get_field('achievement_type')=='Inducted') {?>
             <div class="show-more post-txt">Inducted: "<?php the_title(); ?>, <?php echo get_the_date('F, Y'); ?> ," <?php if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div>
          <?php } elseif (get_field('achievement_type')=='Served') {?>
             <div class="show-more post-txt">Served: "<?php the_title(); ?>", <?php the_field('served_start'); ?> to <?php the_field('served_end'); ?> </div>
          <?php } elseif (get_field('achievement_type')=='Consulted') {?>
             <div class="show-more post-txt">Consulted: "<?php the_title(); ?>, <?php echo get_the_date('F, Y'); ?> ," <?php if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div>
          <?php } elseif (get_field('achievement_type')=='Organized') {?>
             <div class="show-more post-txt">Organized: "<?php the_title(); ?>, <?php echo get_the_date('F, Y'); ?> ," <?php if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div>
          <?php } elseif (get_field('achievement_type')=='Patents') {?>
             <div class="show-more post-txt">Patents: "<?php the_title(); ?>," <?php if (get_field('collaborators')!='') {?>, <?php the_field('collaborators'); ?>.<?php } else { echo ".";} ?></div>
          <?php } else { ?>
             <div class="show-more post-txt"><?php the_title(); ?></div> 
          <?php } ?>
             
          <div class="more">MORE</div>
      </div>


<?php    endwhile; ?>
</div>
<?php endif; 
  wp_reset_query();
?>


  
      <br class="float_clear"/>
    </div>

</div>
<?php
get_footer(); ?>
