<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<!-- Left content column -->
    <div id="leftContent">
      <div id="pageIdentity"> 
        <!-- Breadcrumbs -->
        <div id="breadcrumbs">
          <p><a href="http://www.marquette.edu">Marquette.edu</a> // <a href="#">Website name</a> // </p>
        </div>
         <!-- Page name -->
        <div id="pageName">
     		  <h1>404</h1>
     </div>
    </div>
	 <p>page not found!</p>

    </div> 
       
    <!-- End left content --> 
    
    <!-- Start sidebar content -->
    <div id="sidebarRightImage">
      <div id="pageImage">
     
      <?php if ( has_post_thumbnail() ) { the_post_thumbnail('single_page-thumb'); 
	  		} else { ?>
	  		<img src="<?php bloginfo('template_directory'); ?>/images/ft-img-placeholder.jpg"/>
	  <?php } ?>  
      </div>
      <div id="sidebarRight">
        <div id="columnHeader">
         	<h1>
          <?php $sb_title = get_post_meta($post->ID, 'sidebar_title', true);
		  		  if ($sb_title) {
					echo $sb_title;  
				  } else {
					  echo "Quick links" ;
				  }
				  ?>
          </h1>
        </div>
        <div id="content">

		<?php $cf = get_post_meta($post->ID, 'custom_page_sidebar', true);
				if ($cf){
  				echo $cf;
				} else {
  				if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Page Sidebar') ) :   endif; 
				}
		?>
          
        </div>
      </div>
      <br class="float_clear" />
    </div>
    <!-- End right sidebar --> 
    <br class="float_clear"/>
  </div>

</div>
<?php
get_footer();
