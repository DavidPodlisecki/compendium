<?php
/**
 * Template Name: Search
 * Description: Custom search template
 *
 * @package WordPress
 * @subpackage MU-v2
 * @since MU 1.0
 */

get_header(); ?>
<!-- Left content column -->
    <div id="wideContent">
      <div id="breadcrumbs">
        <p><a href="http://marquette.edu/">Marquette.edu</a> // <a href="<?php echo get_site_url(); ?>">Research and Scholarship</a> //
        <?php if($post->post_parent != false): ?>
        <a href="<?php the_permalink(wp_get_post_parent_id()); ?>"><?php echo get_the_title(wp_get_post_parent_id()); ?></a> //
        <?php endif; ?>
        </p>
      </div>
      <div id="pageName">
        <h1><?php the_title(); ?></h1>

      </div>
       <h1>Research and scholarship activity at Marquette</h1>
    
    
        <!-- Main story item -->
      <?php
      if ( have_posts() ) :
        // Start the Loop.
        while ( have_posts() ) : the_post();

          /*
           * Include the post format-specific template for the content. If you want to
           * use this in a child theme, then include a file called called content-___.php
           * (where ___ is the post format) and that will be used instead.
           */
         the_content();

         ?>
         

         
        <p>
          <input type="text" id="quicksearch" placeholder="Search">
        </p>
      <div id="filters" class="button-group filter-button-group">
       
         
        <div class="filter-wrp">
      <div class="blog-filter">
      <label>College or Department</label>
 
 <?php $wpdcargs = array(

	'show_option_none'   => 'All',
	'option_none_value'  => '*',
	'orderby'            => 'NAME', 
	'order'              => 'ASC',
	'show_count'         => 0,
	'hide_empty'         => 0, 
	'child_of'           => 0,
	'exclude'			 => '1',
	'echo'               => 1,
	'selected'           => 0,
	'hierarchical'       => 1, 
	'name'               => 'college',
	'id'                 => 'college',
	'class'              => 'categories',
	'depth'              => 0,
	'tab_index'          => 0,
	'taxonomy'           => 'category',
	'hide_if_empty'      => false,
	'value_field'	     => 'slug',	
); ?>
 
       
	<?php wp_dropdown_categories( $wpdcargs ); ?>
      </div>  

      <div class="blog-filter">
        <label>Date</label>
        <select name="archive-dropdown" id="archive-dropdown">
  <option value="*"><?php echo esc_attr( __( '*' ) ); ?></option>  
  <?php wp_get_archives( array( 'type' => 'monthly', 'style'  => 'none', 'format' => 'option') ); ?>
</select>
      </div>

      <div class="blog-filter">
        <label>Achievement</label>
        <select name="achievement" id="achievementpulldown" class="">
          <option value="*">All</option>
          <option value=".Appointed">Appointed</option>
          <option value=".Awarded">Awarded</option>
          <option value=".Chaired">Chaired</option>          
          <option value=".Consulted">Consulted</option>
          <option value=".Edited">Edited</option>          
          <option value=".Elected">Elected</option>
          <option value=".Grant">Grant</option>
          <option value=".Inducted">Inducted</option>
          <option value=".Organized">Organized</option>
          <option value=".Patents">Patents</option>
          <option value=".Presented">Presented</option>
          <option value=".Published-Article">Published Article</option>
          <option value=".Published-Book">Published Book</option>
          <option value=".Published-Chapter">Published Chapter</option>
          <option value=".Reviewed">Reviewed</option>
          <option value=".Served">Served</option>
          <option value=".Translated-Article">Translated Article</option>
          <option value=".Translated-Book">Translated Book</option>
          <option value=".Translated-Chapter">Translated Chapter</option>      
        </select>
      </div>  
    </div>
      </div>
      <?php
        endwhile;
      endif;
      ?>

	<div id="noresults" style="padding: 15px 0;"><h3 style="text-align:center;">No results</h3></div>
	
<div id="content" class="isotope"> </div>
  
      <br class="float_clear"/>
    </div>
<a id="inifiniteLoader">Loading... <img src="<?php bloginfo('template_directory'); ?>/images/ajax-loader.gif" /></a>
</div>
<?php
get_footer(); ?>
