<?php
/**
 * The main template file
 *
 * @package WordPress
 * @subpackage MU-v2
 * @since MU 1.0
 */

get_header(); ?>

    <!-- Left content column -->
    <div id="leftContent">
      <div id="newsEvents">
        <h1>News and Events</h1>
      </div>
      <div id="leftStory"> 
        <!-- Main story item -->
<?php

		$newsposts = new WP_Query('posts_per_page=1&cat=-4');
		if ( is_front_page()) { ?>

<?php    if ($newsposts->have_posts()) : while ($newsposts->have_posts()) : $newsposts->the_post();
?>
         <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
		<p><?php wp_excerpt('wp_excerptlength_home', 'wp_excerptmore'); ?></p>
        <p><a href="<?php the_permalink(); ?>">Read more</a></p>



<?php    endwhile; endif; 
 
} 
?>

      </div>
      <!-- Main news -->
      <div id="rightEventsNews">
    <!-- End bottom featured content --> 
		<?php
				

		
			query_posts('posts_per_page=5&offset=1&cat=-4');
			if ( have_posts() ) :
				// Start the Loop.
				while ( have_posts() ) : the_post();

					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */
					get_template_part( 'content', 'rightnews' );

				endwhile;
			endif;
		?>
		 <p> <a href="/latest-news/">More news and events</a></p>
        <br class="float_clear" />
      </div>
      <br class="float_clear" />
    </div>
    <!-- End left content --> 
    
    <!-- Start sidebar content -->
    <div id="sidebarRight">
      <div id="columnHeader">
        <h1>Lorem ipsum </h1>
      </div>
      <div id="content">
        <?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Home Sidebar') ) : ?><?php endif; ?>
        <br class="float_clear" />
      </div>
    </div>
    <!-- End right sidebar --> 
    <br class="float_clear" />
    <!-- Start bottom featured content -->
    
    <div id="bottomContent">
      
      <?php $ft_posts = new WP_Query('posts_per_page=3&cat=4');
		if ( is_front_page()) { ?>
		<?php if ($ft_posts->have_posts()) : while ($ft_posts->have_posts()) : $ft_posts->the_post();?> 
		
        <div class="feature <?php if (!((1+$ft_posts->current_post) % 3)) echo 'last'?>">
        
		<?php the_post_thumbnail( 'ft-post-thumb' ); ?>
		<h2><a href="<?php the_permalink();?>"><?php if (strlen($post->post_title) > 20) {echo substr(the_title($before = '', $after = '', FALSE), 0, 20) . '...'; } else {the_title();} ?></a>
        </h2>

          <p><?php wp_excerpt('wp_excerptlength_ft', 'wp_excerptmore'); ?></p>
        <p><a href="<?php the_permalink(); ?>">Read more</a></p>
</div>
<?php    endwhile; endif; 
 
} 
?>
  
      <br class="float_clear"/>
    </div>


</div>
<?php
get_footer();
