<?php 

add_theme_support( 'post-thumbnails' ); 

// Main Menus
  register_nav_menus( array(
    'primary'   => __( 'Top Right Menu', 'twentyfourteen' ),
    'secondary' => __( 'Home Sidebar nav', 'twentyfourteen' ),
    'sidebar' => __( 'Page Sidebar', 'twentyfourteen' ),
  ) );

// Image sizes
if ( function_exists( 'add_image_size' ) ) { 
  add_image_size( 'header-banner', 1024, 250, true ); //HOME PAGE BANNER (cropped)
  add_image_size( 'ft-post-thumb', 125, 125, true ); //FEATURED POST FEATURED IMAGE (cropped)
  add_image_size( 'single_page-thumb', 350, 250, true ); //SINGLE PAGE FEATURED IMAGE (cropped)
  add_image_size( 'IDK', 1024, 9999, true ); //1024 pixels wide (and unlimited height)
  add_image_size( 'home-ftPost-img', 125, 125, true ); //1024 pixels wide (and unlimited height)
  
}

// Excerpt Sizes
function wp_excerptlength_ft( $length ) {
    
    return 15;
}
function wp_excerptlength_home( $length ) {
    
    return 60;
}
function wp_excerptlength_about( $length ) {
    
    return 120;
}
function wp_excerptmore( $more ) {
    
    return '...';
}


   function wp_excerpt( $length_callback = '', $more_callback = '' ) {
    
    if ( function_exists( $length_callback ) )
        add_filter( 'excerpt_length', $length_callback );
    
    if ( function_exists( $more_callback ) )
        add_filter( 'excerpt_more', $more_callback );
    
    $output = get_the_excerpt();
    $output = apply_filters( 'wptexturize', $output );
    $output = apply_filters( 'convert_chars', $output );
    $output = '' . $output . ''; // maybe wpautop( $foo, $br )
    echo $output;
}

// Widgets
if ( function_exists('register_sidebar') )
    register_sidebar( array(
   'name' => __( 'Home Sidebar'),
   'id' => 'home_sidebar',
   'description' => __( 'home Sidebar', 'twentyfourteen' ),
   'before_widget' => '<aside id="%1$s" class="widget %2$s">',
   'after_widget' => "</aside>",

   ) );


if ( function_exists('register_sidebar') )
    register_sidebar( array(
   'name' => __( 'Page Sidebar'),
   'id' => 'page_sidebar',
   'description' => __( 'Page Sidebar', 'twentyfourteen' ),
   'before_widget' => '<aside id="%1$s" class="widget %2$s">',
   'after_widget' => "</aside>",

   ) );

// NAV WIDGETS  

if ( function_exists('register_sidebar') )
    register_sidebar( array(
   'name' => __( 'Nav Widget One'),
   'id' => 'nav_widget_one',
   'description' => __( 'Nav Widget One', 'twentyfourteen' ),
   'before_widget' => '',
   'after_widget' => "",

   ) );
   
if ( function_exists('register_sidebar') )
    register_sidebar( array(
   'name' => __( 'Nav Widget Two'),
   'id' => 'nav_widget_two',
   'description' => __( 'Nav Widget Two', 'twentyfourteen' ),
   'before_widget' => '',
   'after_widget' => "",

   ) );
 
 if ( function_exists('register_sidebar') )
    register_sidebar( array(
   'name' => __( 'Nav Widget Three'),
   'id' => 'nav_widget_three',
   'description' => __( 'Nav Widget Three', 'twentyfourteen' ),
   'before_widget' => '',
   'after_widget' => "",

   ) );
   
 if ( function_exists('register_sidebar') )
    register_sidebar( array(
   'name' => __( 'Nav Widget Four'),
   'id' => 'nav_widget_four',
   'description' => __( 'Nav Widget Four', 'twentyfourteen' ),
   'before_widget' => '',
   'after_widget' => "",

   ) );   
   
 if ( function_exists('register_sidebar') )
    register_sidebar( array(
   'name' => __( 'Nav Widget Five'),
   'id' => 'nav_widget_five',
   'description' => __( 'Nav Widget Five', 'twentyfourteen' ),
   'before_widget' => '',
   'after_widget' => "",

   ) );   
   
  if ( function_exists('register_sidebar') )
    register_sidebar( array(
   'name' => __( 'Nav Widget Six'),
   'id' => 'nav_widget_six',
   'description' => __( 'Nav Widget Six', 'twentyfourteen' ),
   'before_widget' => '',
   'after_widget' => "",

   ) );   
   
   if ( function_exists('register_sidebar') )
    register_sidebar( array(
   'name' => __( 'Nav Widget Seven'),
   'id' => 'nav_widget_Seven',
   'description' => __( 'Nav Widget Seven', 'twentyfourteen' ),
   'before_widget' => '',
   'after_widget' => "",

   ) );

  if ( function_exists('register_sidebar') )
    register_sidebar( array(
   'name' => __( 'Custom Footer'),
   'id' => 'custom_footer',
   'description' => __( 'Custom Footer', 'twentyfourteen' ),
   'before_widget' => '',
   'after_widget' => "",

   ) );
    
   
   

// Theme Header Image
 $header_args = array(
 'flex-height' => false,
 'height' => 250,
 'flex-width' => false,
 'width' => 1024,
 'default-image' => '%s/images/campus-main.jpg',
 'admin-head-callback' => 'mytheme_admin_header_style',
 'default-text-color' => '',
 'header-text' => false,
 'uploads' => true,
 'wp-head-callback' => '',
 'admin-head-callback' => '',
 'admin-preview-callback' => '',
 );
 
 add_theme_support( 'custom-header', $header_args );
 

/** Removes mismatched </p> and <p> tags from a string */
function copter_remove_crappy_markup( $string )
{
    $patterns = array(
        '#^\s*</p>#',
        '#<p>\s*$#'
    );

    return preg_replace($patterns, '', $string);
}

// Shortcodes
function my_shortcode_information( $attr, $content )
{
    $clean = copter_remove_crappy_markup($content);
    return '<div id="leftInformation">' . $clean . '</div>';
}
add_shortcode('info', 'my_shortcode_information');

function my_shortcode_licolumn( $attr, $content )
{
    $clean = copter_remove_crappy_markup($content);
    return '<div class="li-column"><ul>' . $clean . '</ul></div>';
}
add_shortcode('list-column', 'my_shortcode_licolumn');

/**
 * Change Post to Achievements .
 */
function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Achievements';
    $submenu['edit.php'][5][0] = 'Achievements';
    $submenu['edit.php'][10][0] = 'Add Achievement';
    $submenu['edit.php'][16][0] = 'Achievement Tags';
    echo '';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Achievements';
    $labels->singular_name = 'Achievement';
    $labels->add_new = 'Add Achievement';
    $labels->add_new_item = 'Add Achievement';
    $labels->edit_item = 'Edit Achievement';
    $labels->new_item = 'Achievements';
    $labels->view_item = 'View Achievement';
    $labels->search_items = 'Search Achievements';
    $labels->not_found = 'No Achievements found';
    $labels->not_found_in_trash = 'No Achievements found in Trash';
    $labels->all_items = 'All Achievements';
    $labels->menu_name = 'Achievements';
    $labels->name_admin_bar = 'Achievements';
}
 
add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );

/**
 * Add work custom post type.
 */
/**
*add_action( 'init', 'create_proposal_type' );
*function create_proposal_type() {
*  register_post_type( 'sif-pre-proposal',
*    array(
*      'labels' => array(
*        'name' => __( 'SIF Pre-Proposals' ),
*        'singular_name' => __( 'SIF Pre-Proposal' ),
*        'add_new_item' => __( 'Add New SIF Pre-Proposal' ),
*        'edit_item' => __( 'Edit SIF Pre-Proposal' )
*      ),
*      'supports' => array(
*      ),
*      'taxonomies' => array('category'),
*    'can_export' => true,
*    'public' => true,
*    'has_archive' => false,
*    'menu_position' => 5
*    )
*  );
*}
*/
class CatWalk extends Walker_Category {

    function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ) {

        extract($args);



        $cat_name = esc_attr( $category->name );

        $cat_name = apply_filters( 'list_cats', $cat_name, $category );

        $cat_slug = esc_attr( $category->slug );

  

        // ---  

  $termchildren = get_term_children( $category->term_id, $category->taxonomy );

        if(count($termchildren)>0){

      $aclass =  ' class="parent" ';

        }



        //$link = '<a '.$aclass.' href="' . esc_url( get_term_link($category) ) . '" ';

  // ---

  

      /*  if ( $use_desc_for_title == 0 || empty($category->description) )

            $link .= 'title="' . esc_attr( sprintf(__( 'View all posts filed under %s' ), $cat_name) ) . '"';

        else

            $link .= 'title="' . esc_attr( strip_tags( apply_filters( 'category_description', $category->description, $category ) ) ) . '"';

            $link .= '>';

            $link .= $cat_name . '</a>'; */



        if ( !empty($show_count) )

            $link .= ' (' . intval($category->count) . ')';

                   $thisisasubcat = false;

                if ( 'list' == $args['style'] ) {

                        $output .= "\t<li";

                        $class = 'cat-item cat-item-' . $category->term_id;

                                $_current_category = get_term( $current_category, $category->taxonomy );


                        if ( !empty($current_category) ) {


                                if ( $category->term_id == $current_category )

                                        $class .=  ' current-cat';

                                elseif ( $category->term_id == $_current_category->parent )

                                        $class .=  ' current-cat-parent';
                                       $thisisasubcat = true;



                        }

                        $output .=  ' class="' . $class . '"';

                        $output .= ">$link\n";

                } else {

                        if ($thisisasubcat)
                        {
                                                  $output .= "\t<option class=\"$class \" value=\".category-$cat_slug\">   •  $cat_name</option>\n";

                        }
                          else
                          {
                        $output .= "\t<option class=\"$class \" value=\".category-$cat_slug\">$cat_name</option>\n";
                          }
                }

        }

    } function get_cat_slug($cat_id) {
       $cat_id = (int) $cat_id;
       $category = &get_category($cat_id);
       return $category->slug;
}
 
function the_titlesmall($before = '', $after = '', $echo = true, $length = false) { $title = get_the_title();

$title = strip_tags($title);

  if ( $length && is_numeric($length) ) {
    $title = substr( $title, 0, $length );
  }

  if ( strlen($title)> 0 ) {
    $title = apply_filters('the_titlesmall', $before . $title . $after, $before, $after);
    if ( $echo )
      echo $title;
    else
      return $title;
  }
}
function listtheauthors() {
  global $wpdb;
  $authors = $wpdb->get_results("SELECT ID, user_nicename from $wpdb->users WHERE display_name <> 'admin' ORDER BY display_name");
  // echo "<pre>"; var_dump($authors); echo "</pre>";
  echo "<div class='author-wrp isotopeFaculty'>" ;
  foreach ($authors as $author ) {
  // echo "<pre>"; var_dump($author); echo "</pre>";
  $acfuserid = 'user_' . $author->ID; 
  $k = $style_counter%2; echo $style_classes[$k]; $style_counter++;
     $catSlug = get_user_meta($author->ID,'_author_cat',true);
     
$level = get_user_meta($author->ID, 'wp_capabilities', true);


if($level[author] == TRUE) { // not admin


     
     
  if (empty($catSlug) || count($catSlug) <= 0 || !is_array($catSlug))
  {
    
  }
  else
  { 
    $cat = get_user_meta($author->ID,'_author_cat',true);
    $autCatD = get_cat_slug( $cat[0] ); 
    $autCatS = get_cat_slug( $cat[1] ); 
  } 

  //echo "<div style='width:100%;'>" . ($cat[0]) . " <br/>" . get_cat_slug( $cat[0] ) . "</div>";

  echo "<div class='author faculty post-$k category-$autCatD category-$autCatS'";

  echo "<a href=\"".get_bloginfo('url')."/author/";

  the_author_meta('user_nicename', $author->ID);
  echo "/\">";
   echo '<img src="';
   if( get_field('photo', $acfuserid) ){
   echo get_field('photo', $acfuserid)["url"];

   }else {
   echo "http://2.gravatar.com/avatar/ef4cea9b59309cdd8ad9099d1211f00d?s=160&d=mm&r=g";
   }
   echo '" border="0" alt="" align="left" class="profilePhoto" width="110" height="110" />';

  echo "</a><br>";

  echo "<a href=\"".get_bloginfo('url')."/author/";
  the_author_meta('user_nicename', $author->ID);
  echo "/\">";
  the_field('prefix', $acfuserid);
  echo " ";
  //echo $level;
  echo " "; 
  the_author_meta('display_name', $author->ID);
  echo "</a><br>"; 
   
  the_field('title', $acfuserid);
  echo "</br>";   
  
  $cat = get_user_meta($author->ID,'_author_cat',true);
  if (empty($cat) || count($cat) <= 0 || !is_array($cat))
  {
    echo "none";
  }
  else
  { 
    echo get_cat_name( $cat[0] ); 
  
  }
    
  echo "<br>";
  the_field('phone', $acfuserid);

  echo "<br>";

  the_author_meta('user_email', $author->ID);

  echo "</div>";

  }
   } // not admin  
   echo "</div>";
   

}

add_shortcode( 'listauthors', 'listtheauthors' );


//begin achievements functions

function wp_infinitepaginate(){ 
    $loopFile        = $_POST['loop_file'];
    $paged           = $_POST['page_no'];
    $posts_per_page  = get_option('posts_per_page');
	 if ($_POST["posts"]==-1) {
		query_posts(array('nopaging' => true ));   
	}else{  
		query_posts(array('paged' => $paged ));
	}
	 
 
 
    # Load the posts
     
    get_template_part( $loopFile );
 
    exit;
}

add_action('wp_ajax_infinite_scroll', 'wp_infinitepaginate');           // for logged in user
add_action('wp_ajax_nopriv_infinite_scroll', 'wp_infinitepaginate');    // if user not logged in


add_action( 'rest_api_init', 'register_custom' );

function register_custom() {
    register_rest_field( 'post',
        'author_profile',
        array(
            'get_callback'    => 'slug_get_author_profile',
            'update_callback' => null,
            'schema'          => null,
        )
    );
    register_rest_field( 'post',
        'author_acf',
        array(
            'get_callback'    => 'slug_get_author_acf',
            'update_callback' => null,
            'schema'          => null,
        )
    );
    register_rest_field( 'post',
        'author_cats',
        array(
            'get_callback'    => 'slug_get_author_cats',
            'update_callback' => null,
            'schema'          => null,
        )
    );
    register_rest_field( 'post',
        'mu_author_cats',
        array(
            'get_callback'    => 'mu_slug_get_author_cats',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}

function slug_get_author_profile( $object, $field_name, $request ) {
    $returnable = get_user_by("ID", $object["author"]);
    return $returnable;
}

function slug_get_author_acf( $object, $field_name, $request ) {
    $returnable = get_fields('user_' . $object["author"]);
    return $returnable;
}

function slug_get_author_cats( $object, $field_name, $request ) {
    $cats = array();
    foreach ($object["categories"] as $value) {
      $cats[] = get_category($value);
    }
    return $cats;
}

function mu_slug_get_author_cats( $object, $field_name, $request ) {
    $cats = "";
    foreach ($object["categories"] as $value) {
      $cats = $cats . get_category($value)->name . ",";
      // $cats = "test";
    }
    return $cats;
}

function my_searchwp_extra_metadata( $extra_meta, $post_being_indexed ) {
    
    // available author meta: http://codex.wordpress.org/Function_Reference/get_the_author_meta
    
    // retrieve the author's name(s)
    $author_nicename      = get_the_author_meta( 'user_nicename', $post_being_indexed->post_author );
    $author_display_name  = get_the_author_meta( 'display_name', $post_being_indexed->post_author );
    $author_nickname      = get_the_author_meta( 'nickname', $post_being_indexed->post_author );
    $author_first_name    = get_the_author_meta( 'first_name', $post_being_indexed->post_author );
    $author_last_name     = get_the_author_meta( 'last_name', $post_being_indexed->post_author );
    
    // grab the author bio
    $author_bio           = get_the_author_meta( 'description', $post_being_indexed->post_author );
    
    // index the author name and bio with each post
    $extra_meta['my_author_meta_nicename']     = $author_nicename;
    $extra_meta['my_author_meta_display_name'] = $author_display_name;
    $extra_meta['my_author_meta_nickname']     = $author_nickname;
    $extra_meta['my_author_meta_first_name']   = $author_first_name;
    $extra_meta['my_author_meta_last_name']    = $author_last_name;
    $extra_meta['my_author_meta_bio']          = $author_bio;
    
    return $extra_meta;
} 
add_filter( 'searchwp_extra_metadata', 'my_searchwp_extra_metadata', 10, 2 );

function my_searchwp_author_meta_keys( $keys )
{
    // the keys we used to store author meta (see https://gist.github.com/jchristopher/8558947 for more info)
    $my_custom_author_meta_keys = array( 
        'my_author_meta_nicename', 
        'my_author_meta_display_name', 
        'my_author_meta_nickname', 
        'my_author_meta_first_name', 
        'my_author_meta_last_name', 
        'my_author_meta_bio' 
    );
    
    // merge my custom meta keys with the existing keys
    $keys = array_merge( $keys, $my_custom_author_meta_keys );
    
    // make sure there aren't any duplicates
    $keys = array_unique( $keys );
    
    return $keys;
}
 
add_filter( 'searchwp_custom_field_keys', 'my_searchwp_author_meta_keys', 10, 1 );

/**
 * Adds the individual sections, settings, and controls to the theme customizer
 */
function example_customizer404( $wp_customize ) {
  /**
 * Adds textarea support to the theme customizer
 */
class Example_Customize_Textarea_Control extends WP_Customize_Control {
    public $type = 'textarea';
 
    public function render_content() {
        ?>
            <label>
                <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
                <textarea rows="5" style="width:100%;" <?php $this->link(); ?>><?php echo esc_textarea( $this->value() ); ?></textarea>
            </label>
        <?php
    }
}
    $wp_customize->add_section(
        'MobileSettings',
        array(
            'title' => 'Mobile Header Settings',
            'description' => 'Change text for Moble Header',
            'priority' => 35,
        )
    );
    $wp_customize->add_setting( 'MobileHeader_title',
    array(
        'default' => 'Marquette University'
    )

);
    $wp_customize->add_control( 'MobileHeader_title',
    array(
        'label' => 'Header Title',
        'section' => 'MobileSettings',
        'type' => 'text'
    )
);
     $wp_customize->add_setting( 'MobileHeader_link',
    array(
        'default' => 'http://www.marquette.edu'
    )

);
    $wp_customize->add_control( 'MobileHeader_link',
    array(
        'label' => 'Header Title Link',
        'section' => 'MobileSettings',
        'type' => 'text'
    )
);


 $wp_customize->add_setting( 'MobileSubHeader_title',
    array(
        'default' => 'Research and Scholarship'
    )

);
    $wp_customize->add_control( 'MobileSubHeader_title',
    array(
        'label' => 'Header Subtitle',
        'section' => 'MobileSettings',
        'type' => 'text'
    )
);
     $wp_customize->add_setting( 'MobileSubHeader_link',
    array(
        'default' => 'index.php'
    )

);
    $wp_customize->add_control( 'MobileSubHeader_link',
    array(
        'label' => 'Header Subtitle Link',
        'section' => 'MobileSettings',
        'type' => 'text'
    )
);
}
add_action( 'customize_register', 'example_customizer404' );
;


