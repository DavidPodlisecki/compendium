<?php
/**
 * Template Name: Custom
 * Description: Custom page template
 *
 * @package WordPress
 * @subpackage MU-v2
 * @since MU 1.0
 */

get_header(); ?>
<?php if(get_field('slideshow')): ?>
    <div id="mainImage">
      <?php while(has_sub_field('slideshow')): ?>
      <div class="slide">
        <strong><?php the_sub_field('slide-headline'); ?></strong>
        <?php the_sub_field('slide-text'); ?>
       
         <?php $slideImg1 = get_sub_field('slide-img'); if( !empty($slideImg1) ): ?>
          <img src="<?php echo $slideImg1 ['url']; ?>" alt="<?php echo $slideImg1['alt']; ?>" />
          <?php endif; ?>
      </div>
      <?php endwhile; ?>
    </div>
<?php endif; ?>
    <!-- Left content column -->
    <div id="leftContent">
      <div id="newsEvents">
        <h1><?php the_field('text-title'); ?></h1>
      </div>
      <div id="leftStory"> 
        <?php the_field('left_body');?>
      </div>
      <!-- Main news -->
     <div id="rightEventsNews">
    <!-- End bottom featured content --> 
      <?php the_field('right_body');?>
      </div>


      <br class="float_clear" />
    </div>
    <!-- End left content --> 
    
    <!-- Start sidebar content -->
    <div id="sidebarRight">
      <div id="columnHeader">
        <h1><?php the_field('sidebar-title'); ?></h1>
      </div>
      <div id="content">
        <?php the_field('sidebar-content'); ?>
        <br class="float_clear" />
      </div>
    </div>
    <!-- End right sidebar --> 
    <br class="float_clear" />

    <!-- Start bottom featured content -->
   
    <!-- Start bottom featured content -->
  <?php if(get_field('bottom_content')): ?>
    <div id="bottomContent">
      <?php while(has_sub_field('bottom_content')): ?>
      <?php if(get_row_layout() == 'add_feature_articles'): ?>
      <div class="feature">
    
          <?php $image1 = get_sub_field('feature_image1'); if( !empty($image1) ): ?>
          <img src="<?php echo $image1['url']; ?>" alt="<?php echo $image1['alt']; ?>" />
          <?php endif; ?>
                        

        <h2><a href="<?php the_sub_field('feature_article_link1'); ?>"> <?php the_sub_field('feature_article1'); ?></a></h2>
        <p><?php the_sub_field('featured_article_text1'); ?></p>
      </div>
      <div class="feature"> 
        <?php $image2 = get_sub_field('feature_image2'); if( !empty($image2) ): ?>
          <img src="<?php echo $image2['url']; ?>" alt="<?php echo $image2['alt']; ?>" />
          <?php endif; ?>
        <h2><a href="<?php the_sub_field('feature_article_link2'); ?>"> <?php the_sub_field('feature_article2'); ?></a></h2>
        <p><?php the_sub_field('featured_article_text2'); ?></p>
      </div>
      <div class="feature last"> 
        <?php $image3 = get_sub_field('feature_image3'); if( !empty($image3) ): ?>
          <img src="<?php echo $image3['url']; ?>" alt="<?php echo $image3['alt']; ?>" />
          <?php endif; ?>
        <h2><a href="<?php the_sub_field('feature_article_link3'); ?>"> <?php the_sub_field('feature_article3'); ?></a></h2>
        <p><?php the_sub_field('featured_article_text3'); ?></p>
      </div>
      <br class="float_clear">
    <?php endif ?>
  <?php endwhile ?>
    </div>
  <?php endif ?>

</div>
<?php
get_footer(); ?>
